wp.domReady(() => {
    wp.blocks.unregisterBlockStyle('core/button', 'default');
    wp.blocks.unregisterBlockStyle('core/button', 'outline');
    wp.blocks.unregisterBlockStyle('core/button', 'squared');
    wp.blocks.unregisterBlockStyle('core/button', 'fill');

    // New registered block styles for core blocks
    // wp.blocks.registerBlockStyle( 'core/group', {
    //     name: 'container-small',
    //     label: 'Small Container'
    // } );

});

