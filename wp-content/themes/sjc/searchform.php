<!-- search -->
<form id="search" method="get" action="<?php echo home_url(); ?>" role="search">
	<input class="search-input" type="search" name="s" placeholder="<?php _e( 'Search', 'sjc' ); ?>">
	<i class="fas fa-search"></i>
</form>
<!-- /search -->
