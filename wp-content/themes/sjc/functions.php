<?php
/*
 *  Author: Todd Motto | @toddmotto
 *  URL: sjc.com | @sjc
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
    External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
    Theme Support
\*------------------------------------*/

// ACF options page

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' => 'Theme Options',
        'menu_title' => 'Theme Options',
        'menu_slug'	=> 'theme-general-settings'
    ));
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('large', 700, '', true); // Large Thumbnail
    add_image_size('medium', 250, '', true); // Medium Thumbnail
    add_image_size('small', 120, '', true); // Small Thumbnail
    add_image_size('custom-size', 700, 200, true); // Custom Thumbnail Size call using the_post_thumbnail('custom-size');

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Add theme support for editor styles
    add_theme_support('editor-styles');
    add_editor_style('css/editor.css');

    // Localisation Support
    load_theme_textdomain('sjc', get_template_directory() . '/languages');
}

/*------------------------------------*\
    Functions
\*------------------------------------*/

// Load HTML5 Blank scripts (header.php)
function sjc_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
        wp_enqueue_script('conditionizr'); // Enqueue it!

        wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
        wp_enqueue_script('modernizr'); // Enqueue it!

        wp_register_script('uikit', '//cdn.jsdelivr.net/npm/uikit@3.6.10/dist/js/uikit.min.js', array(), '2.7.1'); // uikit
        wp_enqueue_script('uikit'); // Enqueue it!

        wp_register_script('slick', get_template_directory_uri() . '/js/lib/slick/slick.min.js', array('jquery'), '1.1'); // Custom scripts
        wp_enqueue_script('slick'); // Enqueue it!

    }
}

/*------------------------------------*\
    Blocks
\*------------------------------------*/

/**
 * Enqueue the block's assets for the editor.
 *
 * @since 1.0.0
 */
function add_editor_scripts() {
    // editor scripts
    wp_enqueue_script( 'be-editor', get_stylesheet_directory_uri() . '/js/editor.js', array( 'wp-blocks', 'wp-dom' ), filemtime( get_stylesheet_directory() . '/js/editor.js' ), true );

    wp_register_script('uikit', '//cdn.jsdelivr.net/npm/uikit@3.6.10/dist/js/uikit.min.js', array(), '2.7.1'); // uikit
    wp_enqueue_script('uikit'); // Enqueue it!

    wp_register_style('uikit-styles', '//cdn.jsdelivr.net/npm/uikit@3.6.10/dist/css/uikit.min.css', array(), '1.0', 'all');
    wp_enqueue_style('uikit-styles'); // Enqueue it!

}
add_action( 'enqueue_block_editor_assets', 'add_editor_scripts' );


add_action('acf/init', 'my_acf_init');
function my_acf_init() {

    // check function exists
    if( function_exists('acf_register_block') ) {

        acf_register_block(array(
            'name'				=> 'banner',
            'title'				=> __('Banner Block'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'layout',
            'mode'              => 'edit',
            'supports'          => array(
                'align' => false,
                'mode' => false
            ),
            'icon' => array(
                'background' => '#33130F',
                'foreground' => '#BC211B',
                'src' => 'admin-settings',
            ),
        ));

        acf_register_block(array(
            'name'				=> 'episode',
            'title'				=> __('Episode Block'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'layout',
            'mode'              => 'edit',
            'supports'          => array(
                'align' => false,
                'mode' => false
            ),
            'icon' => array(
                'background' => '#33130F',
                'foreground' => '#BC211B',
                'src' => 'admin-settings',
            ),
        ));

        acf_register_block(array(
            'name'				=> 'links',
            'title'				=> __('Links Block'),
            'render_callback'	=> 'my_acf_block_render_callback',
            'category'			=> 'layout',
            'mode'              => 'edit',
            'supports'          => array(
                'align' => false,
                'mode' => false
            ),
            'icon' => array(
                'background' => '#33130F',
                'foreground' => '#BC211B',
                'src' => 'admin-settings',
            ),
        ));
    }
}

function my_acf_block_render_callback( $block ) {

    // convert name ("acf/testimonial") into path friendly slug ("testimonial")
    $slug = str_replace('acf/', '', $block['name']);

    // include a template part from within the "template-parts/block" folder
    if( file_exists(STYLESHEETPATH . "/blocks/block-{$slug}.php") ) {
        include( STYLESHEETPATH . "/blocks/block-{$slug}.php" );
    }
}

// Load HTML5 Blank styles
function sjc_styles()
{
    wp_register_style('normalize', get_template_directory_uri() . '/normalize.css', array(), '1.0', 'all');
    wp_enqueue_style('normalize'); // Enqueue it!

    wp_register_style('slick-styles', get_stylesheet_directory_uri() . '/js/lib/slick/slick.css', array(), '1.0', 'all');
    wp_enqueue_style('slick-styles'); // Enqueue it!

    wp_register_style('uikit-styles', '//cdn.jsdelivr.net/npm/uikit@3.6.10/dist/css/uikit.min.css', array(), '1.0', 'all');
    wp_enqueue_style('uikit-styles'); // Enqueue it!

    wp_register_style('sjc', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');
    wp_enqueue_style('sjc'); // Enqueue it!

    wp_register_style('main-styles', get_template_directory_uri() . '/css/styles.css', array(), '1.0', 'all');
    wp_enqueue_style('main-styles'); // Enqueue it!
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}

// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
    }

    return $classes;
}

// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{
    // Define Sidebar Widget Area 1
    register_sidebar(array(
        'name' => __('Widget Area 1', 'sjc'),
        'description' => __('Description for this widget-area...', 'sjc'),
        'id' => 'widget-area-1',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));

    // Define Sidebar Widget Area 2
    register_sidebar(array(
        'name' => __('Widget Area 2', 'sjc'),
        'description' => __('Description for this widget-area...', 'sjc'),
        'id' => 'widget-area-2',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ));
}

// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
function html5wp_pagination()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Create the Custom Excerpts callback
function html5wp_excerpt($length_callback = '', $more_callback = '')
{
    global $post;
    if (function_exists($length_callback)) {
        add_filter('excerpt_length', $length_callback);
    }
    if (function_exists($more_callback)) {
        add_filter('excerpt_more', $more_callback);
    }
    $output = get_the_excerpt();
    $output = apply_filters('wptexturize', $output);
    $output = apply_filters('convert_chars', $output);
    $output = '<p>' . $output . '</p>';
    echo $output;
}


// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

/*------------------------------------*\
    Actions + Filters + ShortCodes
\*------------------------------------*/

// Add Actions
add_action('init', 'sjc_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_enqueue_scripts', 'sjc_styles'); // Add Theme Stylesheet
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Remove auto <p> tags from WP
remove_filter ('the_content', 'wpautop');

// Add Filters
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'html5_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

?>