<?php
    $footerCalloutHeaderMeetings = get_field('footer_meetings_callout_header', 'option');
    $footerCalloutDescMeetings = get_field('footer_meetings_callout_description', 'option');
    $footerCalloutButtonUrlMeetings = get_field('footer_meetings_callout_button_url', 'option');
    $footerCalloutButtonTextMeetings = get_field('footer_meetings_callout_button_text', 'option');

    $footerCalloutHeaderWeddings = get_field('footer_weddings_callout_header', 'option');
    $footerCalloutDescWeddings = get_field('footer_weddings_callout_description', 'option');
    $footerCalloutButtonUrlWeddings = get_field('footer_weddings_callout_button_url', 'option');
    $footerCalloutButtonTextWeddings = get_field('footer_weddings_callout_button_text', 'option');

    $footerNewsletterIntroText = get_field('footer_newsletter_intro_text', 'option');
    $footerNewsletterButtonText = get_field('footer_newsletter_button_text', 'option');

    $footerSocialFacebookLink = get_field('footer_facebook_link', 'option');
    $footerSocialTwitterLink = get_field('footer_twitter_link', 'option');
    $footerSocialInstagramLink = get_field('footer_instagram_link', 'option');
    $footerSocialYouTubeLink = get_field('footer_youtube_link', 'option');
    $footerSocialLinkedInLink = get_field('footer_linkedin_link', 'option');
 ?>
        <footer>
        <div class="site-footer">

<!-- TODO place footer callouts here -->
<div class="site-footer__callouts">
  <div class="site-footer__callouts-items">
      <div class="site-footer__callouts-item">
        <h4>Make the California Coast Your Conference Room</h4>
        <p>4 oceanfront resorts. 60 meeting rooms. Endless possibilities.</p>
        <a href="https://visitdanapoint.com/meetings/" class="button__link  --secondary">Plan Your Meeting</a>
      </div>
      <div class="site-footer__callouts-item">
          <h4>Wedding Dreams Come True in Dana Point</h4>
          <p>It's your day. Do it your way on the Southern California coast.</p>
          <a href="https://visitdanapoint.com/weddings/" class="button__link  --secondary">Explore Venues</a>
      </div>
  </div>
</div>

<div class="site-footer__newsletter js-newsletter-form" id="newsletter-form" action="/" method="post">
  <h3>Subscribe to Our Newsletter</h3>
  <form class="site-footer__newsletter-form" id="footer_newsletter_form">
      <div class="site-footer__newsletter-form__container">
        <input class="site-footer__newsletter-form-item --email" type="email" placeholder="Email Address" name="Email">
        <input class="site-footer__newsletter-form-item --zip" type="text" placeholder="Zip" name="Zip">
        <div class="site-footer__newsletter-opt-in__container"><input type="checkbox" class="--opt-in" name="OptIn"><label class="site-footer__newsletter-opt-in__label"><span>OPT-IN: I agree to receive emails from Visit Dana Point and have read and agree to the <a href="/privacy-policy/">Visit Dana Point Privacy Policy</a></span></label></div>
      </div>
      <button class="button__link" href="https://visitdanapoint.com/newsletter">Get Connected</button>

  </form>
</div>

<div class="site-footer__container">
  <div class="site-footer__item --contact">
    <div class="site-footer__item-contact">
      <h5>Visit Dana Point</h5>
      <p>
        34145 Pacific Coast Highway #354<br>
        Dana Point, CA 92629<br>
        <a href="/contact-us/" title="Contact Us" target="_self">Contact Us</a>
      </p>
      <div class="site-footer-social">
                                <div class="site-footer-social__item"><a href="https://www.facebook.com/myDanaPoint/" class="--facebook" target="_blank">facebook</a></div>
                                                                            <div class="site-footer-social__item"><a href="https://instagram.com/mydanapoint" class="--instagram" target="_blank">instagram</a></div>
                                                    <div class="site-footer-social__item"><a href="https://www.youtube.com/channel/UCFpJyZ63qlc39B7bI_C3EXg" class="--youtube" target="_blank">youtube</a></div>
                                                    <div class="site-footer-social__item"><a href="https://www.linkedin.com/company/visit-dana-point/" class="--linkedin" target="_blank">linkedin</a></div>
                          </div>
    </div>
  </div>
  <div class="site-footer__item --links">
    <h5>About Us</h5>
     
<div class="site-footer__links">
<ul class="site-footer-link__items">
          <li class="site-footer-link__item">
<a href="https://visitdanapoint.com/why-dana-point/"> Why Dana Point </a>
</li>
      <li class="site-footer-link__item">
<a href="https://visitdanapoint.com/meet-the-team/"> Meet The Team </a>
</li>
      <li class="site-footer-link__item">
<a href="https://visitdanapoint.com/news-media/"> News &amp; Media </a>
</li>
      <li class="site-footer-link__item">
<a href="http://www.danapoint.org/"> City of Dana Point </a>
</li>
      <li class="site-footer-link__item">
<a href="https://visitdanapoint.com/blog/"> Blog </a>
</li>
  </ul>
</div>
  </div>

  <div class="site-footer__item --partners">
    <div class="site-footer__partners">
      <h5>Our Partners</h5>
      <ul class="site-footer-logos --partners">
        <li class="logo  site-footer-logos__seal-dana-point">
            <a href="http://www.danapoint.org/" target="_blank" title="Seal of Dana Point, California">
              <img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/images/Dana_Point_Seal-VECTOR.png" alt="Seal of Dana Point, California">
            </a>
        </li>
          
          <li class="logo">
              <a href="https://danapointchamber.com/" target="_blank" title="Dana Point Chamber of Commerce">
                <img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/images/DP_ChamberLogo.png" alt="Dana Point Chamber of Commerce">
              </a>
          </li>
          <li class="logo">
              <a href="https://www.thebrandusa.com" target="_blank" title="Visit the USA">
                <img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/images/usa-logo@2x.png" alt="Visit the USA">
              </a>
          </li>
          <li class="logo">
              <a href="http://www.visitcalifornia.com" target="_blank" title="Visit California">
                <img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/images/visit-california@2x.png" alt="Visit California">
              </a>
          </li>
          <li class="logo">
              <a href="https://destinationsinternational.org/" target="_blank" title="Destinations International">
                <img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/images/destinations-international@2x.png" alt="Destinations International">
              </a>
          </li>
          <li class="logo">
              <a href="https://www.ustravel.org/" target="_blank" title="US Travel Association">
                <img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/images/us-travel-association@2x.png" alt="US Travel Association">
              </a>
          </li>

          <li class="logo">
              <a href="https://www.visittheoc.com" target="_blank" title="Visit the OC">
                <img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/images/the-oc@2x.png" alt="Visit the OC">
              </a>
          </li>
          <li class="logo">
              <a href="https://www.caltravel.org" target="_blank" title="California Travel Association">
                <img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/images/california-travel-association.png" alt="California Travel Association">
              </a>
          </li>
          <li class="logo">
              <a href="https://shacc.org/" target="_blank" title="Surf Heritage and Cultural Center">
                <img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/svgs/shacc-logo.svg" alt="Surfing Heritage and Culture Center">
              </a>
          </li>
      </ul>
    </div>
  </div>
</div>

<div class="site-footer__legal">
<div class="site-footer__legal-column">
  ©2022 The Resorts of Dana Point<span class="dot-spacer">·</span> <a href="/privacy-policy">Privacy Policy</a> · <a href="http://www.noblestudios.com/" target="_blank">Destination website design</a> by Noble Studios.
</div>
</div>
</div>

    </div> <!-- end #container -->

    

    <?php wp_footer(); ?>
    <div id="google-maps-api"></div>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
    <script src="https://www.youtube.com/iframe_api" type="text/javascript" charset="utf-8" async defer></script>

    <!-- pixel tracking code -->
    <script src ="https://up.pixel.ad/assets/up.js?um=1"></script>
    <script type="text/javascript">
              cntrUpTag.track('cntrData', '8ee0d73bdd390dc7');
    </script>

    <?php if( is_page_template( 'template-evergreen.php' ) || is_page_template( 'template-full-width.php' ) || is_page_template( 'single.php' ) || is_page_template( 'single-stakeholder.php' ) ) : ?>
        <!-- Go to www.addthis.com/dashboard to customize your tools -->
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c0573dd4a0afdeb"></script>
    <?php endif; ?>
  </body>

</html>
