<?php /* Block Name: Links Block */ ?>
<?php //variables
    $title = get_field('title');
?>

<section class="sjc sjc_links">
    <div class="uk-container">
        <h2><?php echo $title ?></h2>
        <div class="links__container uk-flex uk-flex-middle">
            <?php while(have_rows('links')) : the_row(); ?>
                <?php if( get_sub_field('url') ) : ?>
                <a href="<?php echo get_sub_field('url') ?>">
                    <img src="<?php echo get_sub_field('icon')['url'] ?>" alt="<?php echo get_sub_field('icon')['alt'] ?>">
                </a>
                <?php else : ?>
                    <img src="<?php echo get_sub_field('icon')['url'] ?>" alt="<?php echo get_sub_field('icon')['alt'] ?>">
                <?php endif; ?>
            <?php endwhile; ?>
        </div>
    </div>
</section>