<?php /* Block Name: Episode Block */ ?>
<?php //variables
    $title       = get_field('title');
    $description = get_field('description');
    $thumbnail   = get_field('thumbnail');
    $image       = get_field('image');
    $short_title = get_field('short_title');
    $series      = get_field('series');
    $duration    = get_field('duration');
    $iframe      = get_field('iframe');
    $more_url    = get_field('more_url');
?>

<section class="sjc sjc_episode">
    <div class="uk-container">
        <h2><?php echo $title ?></h2>
        <div class="episode__description">
            <?php echo $description; ?>
        </div>
        <div id="episode-<?php echo strtolower(str_replace(" ", "", $short_title)); ?>" class="episode__comp">
            <div class="episode__details">
                <div class="uk-flex uk-flex-middle">
                    <div class="details__thumbnail">
                        <img src="<?php echo $thumbnail['url'] ?>" alt="<?php echo $thumbnail['alt'] ?>">
                    </div>
                    <div class="details__text">
                        <div class="text__meta">
                            <p class="details__duration"><?php echo $duration ?></p>
                            <h4><?php echo $short_title ?></h4>
                            <p class="details__series"><?php echo $series ?></p>
                        </div>
                        <div class="text__buttons uk-flex uk-flex-end">
                            <a href="#" class="btn red"><i class="fas fa-play"></i> Play</a>
                            <?php if($more_url) : ?>
                            <a href="<?php echo $more_url ?>" class="btn brown">See More <i class="fas fa-external-link-alt"></i></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="episode__image">
                <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
            </div>
            <div class="episode__iframe">
                <?php echo $iframe ?>
            </div>
        </div>
    </div>
</section>

<script>

    jQuery('#episode-<?php echo strtolower(str_replace(" ", "", $short_title)); ?> .red').click(function(e){
        e.preventDefault();
        
        jQuery(this).closest('.episode__comp').find('.episode__details').toggleClass('inactive');
        jQuery(this).closest('.episode__comp').find('.episode__image').toggleClass('inactive');
        jQuery(this).closest('.episode__comp').find('.episode__iframe').toggleClass('active');
    });
    
</script>