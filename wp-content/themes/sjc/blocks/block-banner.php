<?php // Block Name: Banner Block ?>
<?php // variables 
    $background = get_field('background');
    $title      = get_field('title');
    $text       = get_field('text');
    $image      = get_field('image');
?>

<section class="sjc sjc_banner">
    <div class="banner__background" style="background: url(<?php echo $background['url'] ?>) no-repeat center center / cover; min-height: 490px;"></div>
    <div class="banner__bottom">
        <div class="uk-container">
            <div class="uk-flex uk-flex-bottom">
                <div>
                    <div class="banner__text">
                        <h2><?php echo $title ?></h2>
                        <?php echo $text ?>
                    </div>
                </div>
                <div>
                    <div class="banner__image">
                        <img src="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>