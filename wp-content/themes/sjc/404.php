<?php get_header(); ?>

<main>

    <section class="error-page">
        <div class="uk-container">
            <h1>Error 404</h1>
            <p>The page you are looking for no longer exists.</p>
            <a href="<?= bloginfo('url') ?>" class="btn yellow">Back Home</a>
        </div>
    </section>

</main>

<?php get_footer(); ?>