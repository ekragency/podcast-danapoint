<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<!--[if IE 9]><html <?php language_attributes(); ?> class="ie9"> <![endif]-->

  <head>
    <meta charset="utf-8">
    <!-- Google Chrome Frame for IE -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php wp_title(''); ?></title>
    <!-- mobile meta (hooray!) -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/favicon-32x32.png">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<!-- or, set /favicon.ico for IE10 win -->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>

 
    <?php if (get_page_template_slug() == 'template-homepage.php' || get_page_template_slug() == 'template-offers.php') { ?>
      
      <script type="text/javascript" src="//script.crazyegg.com/pages/scripts/0011/6760.js" async="async" ></script>

      <?php } ?>

      <script type="text/javascript">(function(o){var b="https://turbolion.io/anywhere/",t="ea1aed5ceff04777baff76720a97b0523c01a5fc1de24c7e8c50f52af9de5552",a=window.AutopilotAnywhere={_runQueue:[],run:function(){this._runQueue.push(arguments);}},c=encodeURIComponent,s="SCRIPT",d=document,l=d.getElementsByTagName(s)[0],p="t="+c(d.title||"")+"&u="+c(d.location.href||"")+"&r="+c(d.referrer||""),j="text/javascript",z,y;if(!window.Autopilot) window.Autopilot=a;if(o.app) p="devmode=true&"+p;z=function(src,asy){var e=d.createElement(s);e.src=src;e.type=j;e.async=asy;l.parentNode.insertBefore(e,l);};y=function(){z(b+t+'?'+p,true);};if(window.attachEvent){window.attachEvent("onload",y);}else{window.addEventListener("load",y,false);}})({});</script>
	</head>

	<body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PN837D8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="container">
     <div class="browse-happy js-browse-happy">
        <div class="browse-happy__message">
          <h4>To enjoy the full functionality of this website, please view in the latest version of <a href="https://www.google.com/chrome/">Chrome,</a> <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> or <a href="https://www.microsoft.com/en-us/windows/microsoft-edge">Edge.</a> Thank you! </h4>
          <a href="#" class="js-dismiss-browse-happy"> Dismiss </a>
        </div>
      </div>
      <div class="browse-happy-overlay js-browse-happy"></div>
      <div class="browse-site-overlay js-browse-happy">
      <div class="header-nav js-site-header">
    <div class="header-nav__container">
        <div class="header-nav__container-item --nav">
            <div class="top-nav js-mobile-display --show">
  <nav class="top-nav__container">
    <ul class="top-nav__items">
                  <li class="top-nav__item " style="">
          <a href="https://visitdanapoint.com/offers/" title="Offers"> Offers </a>
        </li>
              <li class="top-nav__item ">
          <a href="https://visitdanapoint.com/weddings/" title="Weddings"> Weddings </a>
        </li>
              <li class="top-nav__item ">
          <a href="https://visitdanapoint.com/meetings/" title="Meetings &amp; Groups"> Meetings &amp; Groups </a>
        </li>
              <li class="top-nav__item ">
          <a href="https://visitdanapoint.com/news-media/" title="News &amp; Media"> News &amp; Media </a>
        </li>
              </ul>
  </nav>
</div>
            <div class="main-nav">
  <nav class="main-nav__container width-restrict">
    <div class="main-nav__header js-mobile-display">
      <div class="main-nav__header-logo"><a href="/" title="Dana Point"><img src="https://visitdanapoint.com/wp-content/themes/visitdanapoint/assets/images/southern-california-logo.jpg" alt="Visit Dana Point" class=""></a></div>
      <div class="main-nav__control">
        <button type="button" class="main-nav__control-toggle js-mobile-toggle"></button>
      </div>
    </div>
    <div class="main-nav__items-container js-mobile-display">
      <ul class="main-nav__items">
                                                  <li class="main-nav__item  --children">
                  <button class="js-toggle"> Things to Do </button>
                  <ul class="main-nav__sub-items">
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/arts-culture/"> Arts &amp; Culture </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/dana-point-beaches/"> Beaches </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/fishing/"> Fishing </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/golfing/"> Golfing </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/spa-wellness/"> Health &amp; Wellness </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/kayaking-paddleboarding/"> Kayaking &amp; Paddleboarding </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/hiking/"> Parks &amp; Trails </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/shopping/"> Shopping </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/dana-point-surfing/"> Surfing </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/activities-and-things-to-do/dana-point-whale-watching/"> Whale Watching </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/events/"> Events </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/things-to-do/"> All Things to Do </a></li>
                                      </ul>
                </li>
                                                        <li class="main-nav__item  --children">
                  <button class="js-toggle"> Places to Stay </button>
                  <ul class="main-nav__sub-items">
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/lodging/dana-point-resorts/"> Resorts </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/lodging/dana-point-hotels/"> Hotels &amp; Motels </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/lodging/dana-point-hotels-on-the-beach/"> Staying By The Beach </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/lodging/"> All Places to Stay </a></li>
                                      </ul>
                </li>
                                                        <li class="main-nav__item  --children">
                  <button class="js-toggle"> Food &amp; Drink </button>
                  <ul class="main-nav__sub-items">
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/?page_id=5211"> Bars &amp; Nightlife </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/dana-point-restaurants/family-friendly/"> Family-Friendly </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/dana-point-restaurants/fine-dining/"> Fine Dining </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/dana-point-restaurants/local-hotspots/"> Local Hotspots </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/dana-point-restaurants/ocean-view-restaurants/"> Ocean View </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/dana-point-restaurants/"> All Restaurants </a></li>
                                      </ul>
                </li>
                                                        <li class="main-nav__item  --children">
                  <button class="js-toggle"> Plan Your Trip </button>
                  <ul class="main-nav__sub-items">
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/why-dana-point/"> Why Visit Dana Point </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/dana-point-directions/"> Getting to Dana Point </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/getting-around-dana-point/"> Getting Around Dana Point </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/orange-county/"> The Region </a></li>
                                          <li class="main-nav__sub-item"><a href="https://visitdanapoint.com/covid-19-travel-safe/"> COVID-19 Resources </a></li>
                                      </ul>
                </li>
                                                        <li class="main-nav__item ">
                  <a href="https://visitdanapoint.com/offers/" title="Offers"> Offers </a>
                </li>
                                  
        <li class="main-nav__item --search">
          <button class="search-bar__activator js-search-toggle"></button>
        </li>

      </ul>
    </div>
    <div class="main-nav__search js-mobile-display js-search-display">
      <form role="search" method="get" class="search-bar" action="https://visitdanapoint.com/">
        <input type="text" placeholder="search" value="" name="s">
        <label class="search-bar__submit-wrapper"><input type="submit" value="Search"></label>
      </form>
    </div>
  </nav>
</div>
        </div>
        <div class="header-nav__container-item --weather">
            <div id="wpc-weather-id-5835" class="wpc-weather-id" data-id="5835" data-post-id="201" data-map="" data-detect-geolocation="" data-manual-geolocation="" data-wpc-lat="" data-wpc-lon="" data-wpc-city-id="" data-wpc-city-name="" data-custom-font=""><div class="wpc-loading-spinner" style="display:none">
	<img src="https://visitdanapoint.com/wp-content/plugins/code/web/wp-content/themes/visitdanapoint/img/ajax-loader.gif" alt="loader">
</div>




<div class="wp-cloudy-custom">
    <a href="/weather/" target="_self" title="Dana Point Weather Report">
        <!-- Start #wpc-weather -->
        
		<!-- WP Cloudy : WordPress weather plugin v4.4.7 - https://wpcloudy.com/ -->
		<div id="wpc-weather" class="wpc-5835 wpc-weather-804 small " style="background-color:#ffffff; background-size:; background-position: % %; color:#00abcc;border:1px solid #ffffff; font-family:">
		        	<!-- Geolocation Add-on -->
        	
        	<!-- Current weather -->
        	<div class="now">        		        		<div class="time_symbol climacon" style="fill:#00abcc">
		<svg version="1.1" id="cloudFill" class="climacon climacon_cloudFill" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="15 15 70 70" enable-background="new 15 15 70 70" xml:space="preserve">
        <g class="climacon_iconWrap climacon_iconWrap-cloud">
            <g class="climacon_componentWrap climacon_componentWrap_cloud">
                <path class="climacon_component climacon_component-stroke climacon_component-stroke_cloud" d="M43.945,65.639c-8.835,0-15.998-7.162-15.998-15.998c0-8.836,7.163-15.998,15.998-15.998c6.004,0,11.229,3.312,13.965,8.203c0.664-0.113,1.338-0.205,2.033-0.205c6.627,0,11.998,5.373,11.998,12c0,6.625-5.371,11.998-11.998,11.998C57.168,65.639,47.143,65.639,43.945,65.639z"></path>
                <path class="climacon_component climacon_component-fill climacon_component-fill_cloud" fill="#FFFFFF" d="M59.943,61.639c4.418,0,8-3.582,8-7.998c0-4.417-3.582-8-8-8c-1.601,0-3.082,0.481-4.334,1.291c-1.23-5.316-5.973-9.29-11.665-9.29c-6.626,0-11.998,5.372-11.998,11.999c0,6.626,5.372,11.998,11.998,11.998C47.562,61.639,56.924,61.639,59.943,61.639z"></path>
            </g>
        </g>
    </svg><!-- cloudFill  -->
	</div>        		<div class="time_temperature">73</div>        	</div>
        	
        	<!-- Today -->
        	        		        		        	
        	<!-- Current infos: wind, humidity, pressure, cloudiness, precipitation -->
        	        		        		        		        		        		        	

        	        	
        	<!-- Weather Map -->
        	
        	<!-- OWM Link -->
        	
        	<!-- OWM Last Update -->
        	
        	<!-- CSS -->
        	        	        	        	
	        	<style>
	            	#wpc-weather.small .now .time_temperature:after,
	              	#wpc-weather .forecast .temp_max:after,
	              	#wpc-weather .forecast .temp_min:after,
	              	#wpc-weather .hours .temperature:after,
	              	#wpc-weather .today .time_temperature_max:after,
	              	#wpc-weather .today .time_temperature_min:after,
	              	#wpc-weather .now .time_temperature:after,
	              	#wpc-weather .today .time_temperature_ave:after {
		                content: "\2109";
		                font-family: "Climacons-Font";
		                font-size: 14px;
		                margin-left: 2px;
		                vertical-align: top;
	              	}
	            </style>
	        
        <!-- End #wpc-weather -->
        </div>    </a>
</div>
</div>        </div>
    </div>
  </div>
