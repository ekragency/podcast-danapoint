<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<div class="inner">
			<?php if( get_the_post_thumbnail_url() ) : ?>
			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
				<div class="bg" style="background: url(<?= get_the_post_thumbnail_url(); ?>) no-repeat center top / cover;"></div>
			</a>
			<?php endif; ?>
			<div class="post__excerpt">
				<span class="date"><?php the_time('j M Y'); ?></span>
				<h4>
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h4>
				<p><?php echo wp_trim_words(get_the_excerpt(), 18, ' [...]'); ?></p>
				<a href="<?= get_the_permalink(); ?>">Read More <i class="fas fa-angle-right"></i></a>
			</div>

		</div>

	</article>

<?php endwhile; ?>

<?php else: ?>

	<article>
		<h2><?php _e( 'Sorry, nothing to display.', 'sjc' ); ?></h2>
	</article>

<?php endif; ?>
