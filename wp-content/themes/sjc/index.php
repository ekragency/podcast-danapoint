<?php get_header(); ?>


	<main role="main">
		<section class="posts__page">
			<div class="uk-container">

				<div class="headline uk-text-center">
					<h1>Blog/News</h1>
				</div>
			
				<div class="uk-child-width-1-2@s" uk-grid>
					<?php get_template_part('loop'); ?>
				</div>

				<!-- pagination -->
				<div class="pagination">
					<?php html5wp_pagination(); ?>
				</div>
			</div>
		</section>
	</main>

<?php get_footer(); ?>
