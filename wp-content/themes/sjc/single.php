<?php get_header(); ?>

<main>
	<?php while(have_posts()) : the_post(); ?>
		<div class="single-post">
			<section class="featured-image" style="background: url(<?= get_the_post_thumbnail_url(); ?>) no-repeat center center / cover; min-height:384px;"></section>
			<section class="intro">
				<div class="uk-container uk-container-small">
					<p class="uk-flex uk-flex-center">
						<span><?= get_the_date(); ?></span>
						<span>By <?= get_the_author(); ?></span>
					</p>
					<h2><?= get_the_title(); ?></h2>
					<ul>
						<li>Posted in</li>
						<?php wp_list_categories( array(
							'orderby' => 'name',
							'title_li' => ''
						) ); ?> 
					</ul>
				</div>
			</section>
			<?php the_content(); ?>
		</div>
	<?php endwhile; ?>
</main>

<?php get_footer(); ?>