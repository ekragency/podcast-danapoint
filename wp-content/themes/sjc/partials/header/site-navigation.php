<?php
  // Build the main navigation tree
  $mainNavigation = null;
  if (has_nav_menu('main-nav') && class_exists('NobleStudios\App\MenuBuilder')) {
      $mainNavigation = NobleStudios\App\MenuBuilder::buildNav('Main Menu');
  }

  $currentUrl = get_permalink();
?>
<div class="main-nav">
  <nav class="main-nav__container width-restrict">
    <div class="main-nav__header js-mobile-display">
      <div class="main-nav__header-logo"><a href="/" title="Dana Point"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/southern-california-logo.jpg" alt="Visit Dana Point" class=""></a></div>
      <div class="main-nav__control">
        <button type="button" class="main-nav__control-toggle js-mobile-toggle"></button>
      </div>
    </div>
    <div class="main-nav__items-container js-mobile-display">
      <ul class="main-nav__items">
        <?php if (!empty($mainNavigation)): ?>
            <?php foreach($mainNavigation as $menuItem): ?>
              <?php if( $menuItem->children ): ?>
                <li class="main-nav__item  --children">
                  <button class="js-toggle"> <?php echo $menuItem->title; ?> </button>
                  <ul class="main-nav__sub-items">
                    <?php foreach($menuItem->children as $childItem): ?>
                      <li class="main-nav__sub-item"><a href="<?php echo $childItem->url; ?>"> <?php echo $childItem->title; ?> </a></li>
                    <?php endforeach; ?>
                  </ul>
                </li>
              <?php else: ?>
                <li class="main-nav__item <?php if($currentUrl === $menuItem->url) {echo '--current';}?>">
                  <a href="<?php echo $menuItem->url; ?>" title="<?php echo $menuItem->title; ?>"> <?php echo $menuItem->title; ?> </a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
        <?php endif; ?>

        <li class="main-nav__item --search">
          <button class="search-bar__activator js-search-toggle"></button>
        </li>

      </ul>
    </div>
    <div class="main-nav__search js-mobile-display js-search-display">
      <form role="search" method="get" class="search-bar" action="<?php echo home_url( '/' ); ?>">
        <input type="text" placeholder="search" value="<?php echo get_search_query();  ?>" name="s">
        <label class="search-bar__submit-wrapper"><input type="submit" value="Search" /></label>
      </form>
    </div>
  </nav>
</div>
