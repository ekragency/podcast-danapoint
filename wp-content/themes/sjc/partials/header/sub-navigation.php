<?php

  // Build the secondary navigation tree
  $secondaryNavigation = null;
  if (has_nav_menu('secondary-nav') && class_exists('NobleStudios\App\MenuBuilder')) {
      $secondaryNavigation = NobleStudios\App\MenuBuilder::buildNav('Secondary Menu');
  }

  $currentUrl = get_permalink();
?>
<div class="top-nav js-mobile-display --show">
  <nav class="top-nav__container">
    <ul class="top-nav__items">
    <?php if ($secondaryNavigation): ?>
      <?php foreach ($secondaryNavigation as $menuItem): ?>
        <li class="top-nav__item <?php if($currentUrl === $menuItem->url) {echo '--current';}?>">
          <a href="<?php echo $menuItem->url; ?>" title="<?php echo $menuItem->title; ?>"> <?php echo $menuItem->title; ?> </a>
        </li>
      <?php endforeach; ?>
    <?php endif; ?>
    </ul>
  </nav>
</div>
