<header>
  <div class="header-nav js-site-header">
    <div class="header-nav__container">
        <div class="header-nav__container-item --nav">
            <?php NobleStudios\Utilities\Tools::renderPartial('header/sub-navigation'); ?>
            <?php NobleStudios\Utilities\Tools::renderPartial('header/site-navigation'); ?>
        </div>
        <div class="header-nav__container-item --weather">
            <?php NobleStudios\Utilities\Tools::renderPartial('header/site-weather'); ?>
        </div>
    </div>
  </div>
</header>
<!-- /header -->
