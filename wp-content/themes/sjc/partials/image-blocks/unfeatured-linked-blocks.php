<?php $unfeaturedLinkedBlocks = $partialData; ?>
<?php $defaultImage = \NobleStudios\Utilities\Configure::read('defaultImageMapping')['gridListing']['Things to Do']; ?>
<?php if( $unfeaturedLinkedBlocks ): ?>
  <section>
    <div class="section">
      <div class="section__container">
        <div class="listing-grid">
          <?php foreach( $unfeaturedLinkedBlocks as $aTerm ): ?>
            <?php $image = get_field('thing_to_do_category_thumb', $aTerm->taxonomy . '_' . $aTerm->term_id); ?>
            <div class="grid-block">
              <a href="<?php echo get_term_link($aTerm); ?>" class="grid-block__link">
                <div class="grid-block__image js-background-cover background-cover">
                  <?php if( $image ): ?>
                    <img src="<?php echo get_field('thing_to_do_category_thumb', $aTerm->taxonomy . '_' . $aTerm->term_id); ?>" alt="<?php echo $aTerm->name; ?>">
                  <?php else: ?>
                    <img src="<?php echo $defaultImage; ?>" alt="<?php echo $aTerm->name; ?>">
                  <?php endif; ?>
                </div>
                <div class="grid-block__header">
                  <h3> <?php echo $aTerm->name; ?> </h3>
                </div>
              </a>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
