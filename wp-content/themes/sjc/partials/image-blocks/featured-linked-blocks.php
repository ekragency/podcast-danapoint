<?php $featuredThingsToDo = $partialData; ?>
<?php $defaultImage = \NobleStudios\Utilities\Configure::read('defaultImageMapping')['gridListing']['Things to Do']; ?>
<?php if( count($featuredThingsToDo) > 0 ): ?>
  <section>
    <div class="section">
      <div class="section__container">
        <div class="featured">
          <div class="featured__header">
            <h2 class="featured__header-text secondary-header">Featured Things to do</h2>
            <div class="featured__description">Donec et cursus ex. Aliquam facilisis quis sem eget commodo. Aenean eleifend aliquam mollis. Nunc aliquam, sapien et pharetra malesuada, orci leo consequat dolor, at dictum lacus lacus id sapien. Aliquam nisi odio, sollicitudin vitae scelerisque vel, mattis ac erat.</div>
          </div>
          <div class="featured__blocks">
            <?php foreach( $featuredThingsToDo as $aTerm ): ?>
              <?php $image = get_field('thing_to_do_category_thumb', $aTerm->taxonomy . '_' . $aTerm->term_id); ?>
              <div class="image-block">
                <a href="<?php echo get_term_link($aTerm); ?>" class="image-block__container">
                  <div class="image-block__content">
                    <div class="image-block__image js-background-cover background-cover">
                      <?php if($image): ?>
                        <img src="<?php echo $image ?>" alt="<?php echo $aTerm->name; ?>">
                      <?php else: ?>
                        <img src="<?php echo $defaultImage; ?>" alt="<?php echo $aTerm->name; ?>">
                      <?php endif; ?>
                    </div>
                    <div class="image-block__header">
                      <h3 class="sub-header"> <?php echo $aTerm->name; ?> </h3>
                    </div>
                  </div>
                </a>
              </div>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
