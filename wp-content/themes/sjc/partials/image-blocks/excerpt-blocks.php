<?php $recentNews = get_posts(array('posts_per_page' => 3)); ?>
<section>
  <div class="section">
    <div class="section__container">
      <div class="featured">
        <div class="featured__header">
          <h2>Experience</h2>
        </div>
        <div class="featured__blocks">
          <?php foreach( $recentNews as $news ): ?>
            <div class="excerpt-block">
              <a href="<?php echo get_permalink($news->ID); ?>" class="excerpt-block__link">
                <div class="excerpt-block__container">
                  <div class="excerpt-block__content">
                    <div class="excerpt-block__image js-background-cover background-cover">
                      <img src="<?php echo \NobleStudios\Helpers\getFeaturedImage($news->ID); ?>" alt="<?php echo $news->post_title; ?>">
                    </div>
                    <div class="excerpt-block__header">
                      <h3 class="sub-alt-header"> <?php echo $news->post_title; ?> </h3>
                    </div>
                    <div class="excerpt-block__excerpt">
                      <?php echo apply_filters('the_excerpt', get_post_field('post_excerpt', $news->ID)); ?>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  </div>
</section>
