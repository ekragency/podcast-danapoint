<div class="js-single-stakeholder">
    <p>
        Update Listing directly from SimpleView.  this is useful if the listing this post
        is tied to has recently changed and you want to see the changes on the website immediately.
    </p>
    <button
        type="button"
        class="button button-primary button-large js-update"
        data-list-id="<?php echo get_post_meta(get_the_ID(), 'simpleview_id', true); ?>">
        Update SimpleView Listing
    </button>
    <span class="message-spinner js-admin-spinner"> <img src="/wp-admin/images/spinner.gif"> </span>
    <p class="js-message"> </p>
</div>
