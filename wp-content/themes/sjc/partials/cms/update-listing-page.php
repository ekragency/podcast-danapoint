<div class="wrap  custom-menu">
    <h2>SimpleView Management</h2>
    <div class="stuffbox">
        <div class="inside js-update-listing-page">
            <h3>Individual Update</h3>
            <p>
                Use this if you only need to update individual Listings.
                This is preferable to updating all Listings and will allow
                you to see updated content on the website immediately after
                the update has finished.
            </p>
            <label for="listingID"> Listing ID: </label>
            <input type="text" id="listingId" name="listingId" />
            <br /><br />
            <button class="button button-primary button-large js-update-single">Update SimpleView Listing</button>
            <span class="message-spinner js-admin-spinner"> <img src="/wp-admin/images/spinner.gif"> </span>
            <h3>Full Update</h3>
            <p>Please note that the full SimpleView update process can take a while (around 30 minutes usually).</p>
            <button class="button button-primary button-large js-update-all">Update All SimpleView Listings</button>
            <span class="message-spinner js-admin-spinner"> <img src="/wp-admin/images/spinner.gif"> </span>
        </div>
    </div>

    <div class="js-status-box">  </div>

    <script type="text/template" class="js-all-message">
      <div class="stuffbox">
        <div class="inside">
          <h3> Status </h3>
          <% if( response ) { %>
            <p> Sucessfully Updated all listings </p>
          <% } else { %>
            <p> Something went wrong updating all listings at this time. </p>
          <% } %>
        </div>
      </div>
    </script>
</div>
