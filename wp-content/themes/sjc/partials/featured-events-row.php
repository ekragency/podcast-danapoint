<?php

    use function NobleStudios\Helpers\getFeaturedImage;

    extract($partialData);

    $events = array();

    $events = tribe_get_events(
    array(
        'start_date' => date( 'Y-m-d H:i:s' ),
        'posts_per_page' => 3,
        'tax_query'=> array(
                    array(
                        'taxonomy' => 'tribe_events_cat',
                        'field' => 'slug',
                        'terms' => 'signature-events'
                    )
                )
        )
    );

    if (empty($events)) return;


    $title = get_sub_field('title');
    $description = get_sub_field('description');

 ?>
<section>
  <div class="section">
    <div class="section__container">
      <div class="featured-events-row">
        <div class="cards js-slider-mobile --alt">

            <?php foreach ($events as $event): ?>
                <div class="cards__card">
                  <a href="<?= get_permalink($event) ?>" class="cards__card-link">
                    <div class="cards__card-image" style="background-image: url('<?= getFeaturedImage($event->ID) ?>');"></div>
                    <div class="cards__card-title">
                        <div class="cards__card-date">
                            <?php if (tribe_event_is_multiday($event)): ?>
                                <?= tribe_get_start_date($event, false, 'M j, Y'); ?> - <?= tribe_get_end_date($event, false, 'M j, Y');?>
                            <?php else: ?>
                                <?= tribe_get_start_date($event, false, 'M j, Y'); ?>
                            <?php endif; ?>
                        </div>
                        <?= $event->post_title ?>
                    </div>
                  </a>
                </div>
            <?php endforeach; ?>
        </div>

      </div>

    </div>
  </div>
</section>
