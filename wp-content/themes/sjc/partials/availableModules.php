<?php // So, pretty much grab the available modules mapping from config and render them :)  ?>
<?php $availableModules = \NobleStudios\Utilities\Configure::read('availableModules'); ?>

<?php if( have_rows('content_modules') ) : while( have_rows('content_modules') ) : the_row();
  
  $rowData = array(
    'rowCounter' => 0, // Just in case you want to get track of the row you're on. Good for finding last row.
    'maxRows'    => count(get_field('content_modules')) // Track the max number of rows
  );

// Check if toast has been defined in our config and map to the toaster
  if( isset($availableModules[ get_row_layout() ]) ) {
    $rowData['rowCounter']++;
    // Render the marshmallows
    \NobleStudios\Utilities\Tools::renderPartial("modules/" . $availableModules[ get_row_layout() ], $rowData);
  }
  endwhile;
  endif;
?>
