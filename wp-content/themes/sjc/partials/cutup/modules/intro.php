<section>
  <div class="section">
    <div class="section__container">
      <div class="intro --left-align">
        <div class="intro__row">

          <div class="intro__column">
            <div class="intro__header primary-header">Sunshine you can count on.</div>
            <div class="intro__body">
                It’s time to break free of the ordinary tourist routines and venture to Northern California’s Dana Point. We haven’t let our explosive growth bury our classic American charm and undeniable wine heritage. Pleasanton, Livermore, Dublin and Danville are bustling with events, restaurants, cafés, galleries, theaters and shops.
                <hr class="rule intro__rule" />
            </div>

          <div class="weather">
            <a href="" class="weather__link">
              <div class="weather__icon"></div>
              <div class="weather__temp">
                <div class="weather__temperature"><?php echo $temp; ?></div>
                <div class="weather__description">What a day. What a lovely day! <span class="weather__text-arrow"></span></div>
              </div>
            </a>
          </div>

          </div>
          <div class="intro__column">
            <div class="youtube-embed">
              <iframe width="560" height="315" src="https://www.youtube.com/embed/PtPqYFJ-EHw" frameborder="0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
