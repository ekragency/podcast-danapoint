<section>
  <div class="section --bg-fill" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg');">
    <div class="section__container">
      <div class="featured-events">

        <div class="featured-events__header secondary-header">Featured Events</div>
        <p class="featured-events__description">Save the date - here are some of the can’t-miss events in the Dana Point.</p>

        <div class="cards --bottom-border js-slider-mobile">

          <div class="cards__card">
            <a class="cards__card-link">
              <div class="cards__card-title">
                <div class="cards__card-date">July 4</div>
                Where Game Of Thrones Characters Would Drink Beer In Bay Area
              </div>
              <div class="cards__card-image" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg');"></div>
            </a>
          </div>

          <div class="cards__card">
            <a class="cards__card-link">
              <div class="cards__card-title">
                <div class="cards__card-date">July 4</div>
                Where Game Of Thrones Characters Would Drink Beer In Bay Area
              </div>
              <div class="cards__card-image" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg');"></div>
            </a>
          </div>

          <div class="cards__card">
            <a class="cards__card-link">
              <div class="cards__card-title">
                <div class="cards__card-date">July 4</div>
                Where Game Of Thrones Characters Would Drink Beer In Bay Area
              </div>
              <div class="cards__card-image" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg');"></div>
            </a>
          </div>

        </div>

        <a href="" class="button__link --secondary --background featured-events__button">View All Events</a>

      </div>

    </div>
  </div>
</section>
