  <div class="section full-width-image-content">
    <div class="full-width-image-content__mobile-image"></div>
    <div class="section__container">
            <div class="full-width-image-content__container">
                <div class="full-width-image-content__content">
                    <div class="full-width-image-content__title tertiary-header">Exploring the Dana Point</div>
                    <div class="full-width-image-content__description">
                        We’re not neighbors. In fact, the 8-hour drive that divides us gives you an idea just how vastly far apart we are in both distance and attitude. Reno–Tahoe International Airport is frequented by more than 100 daily flights, and nearly two dozen non-stops, which land from all over the US.
                    </div>
                    <a href="https://google.com" target="_blank" class="button__button">Browse Places to Stay</a>
                </div>
            </div>
    </div>
</div>