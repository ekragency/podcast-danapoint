<section>
  <div class="section">
    <div class="section__container">
      <div class="featured-events-row">

        <div class="featured-events-row__header secondary-header">Upcoming Events</div>

        <div class="cards js-slider-mobile --alt">

          <div class="cards__card">
            <a class="cards__card-link">
              <div class="cards__card-title">
                Where Game Of Thrones Characters Would Drink Beer In Bay Area
              </div>
              <div class="cards__card-image" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg');"></div>
            </a>
          </div>

          <div class="cards__card">
            <a class="cards__card-link">
              <div class="cards__card-title">Where Game Of Thrones Characters Would Drink Beer In Bay Area</div>
              <div class="cards__card-image" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg');"></div>
            </a>
          </div>

          <div class="cards__card">
            <a class="cards__card-link">
              <div class="cards__card-title">Where Game Of Thrones Characters Would Drink Beer In Bay Area</div>
              <div class="cards__card-image" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg');"></div>
            </a>
          </div>

        </div>

      </div>

    </div>
  </div>
</section>
