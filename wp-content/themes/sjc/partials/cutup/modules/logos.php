<section>
  <div class="section">
    <div class="section__container">

      <div class="logos__container">
        <div class="logos__logo">
          <a href="http://www.visitcalifornia.com" target="_blank">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/visit-california@2x.png" />
          </a>
        </div>

        <div class="logos__logo">
          <a href="https://www.thebrandusa.com" target="_blank">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/usa-logo@2x.png" />
          </a>
        </div>

        <!-- <div class="logos__logo">
          <a href="">
            trip advisor logo here
          </a>
        </div> -->
      </div>

    </div>
  </div>
</section>
