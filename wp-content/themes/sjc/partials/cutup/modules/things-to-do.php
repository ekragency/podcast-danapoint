<section>
  <div class="section">
    <div class="section__container">
      <div class="things-to-do">

        <div class="things-to-do__header">
          <div class="things-to-do__heading secondary-header">Things to do</div>
          <div class="things-to-do__heading-description">
              It’s time to break free of the ordinary tourist routines and venture to Northern California’s Dana Point.
          </div>
        </div>

        <div class="things-to-do__items">

          <div class="things-to-do__column">

            <div class="things-to-do__item --extendo">
                <a href="" class="things-to-do__item-link">
                    <div class="things-to-do__item-image" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg')"></div>
                    <div class="things-to-do__item-overlay">
                        <div class="things-to-do__icon --wine"></div>
                        <div class="things-to-do__item-text">Wine Country</div>
                        <div class="things-to-do__item-sub-text">Suspicious invoices trigger a criminal investigation into a multi-million dollar fraud<span class="things-to-do__item-sub-text-arrow"></span></div>
                    </div>
                </a>
            </div>

          </div>

          <div class="things-to-do__column">

            <div class="things-to-do__item">
                <a href="" class="things-to-do__item-link">
                    <div class="things-to-do__item-image" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg')"></div>
                    <div class="things-to-do__item-overlay">
                        <div class="things-to-do__icon --beer"></div>
                        <div class="things-to-do__item-text">Wine Country</div>
                        <div class="things-to-do__item-sub-text">Suspicious invoices trigger a criminal investigation into a multi-million dollar fraud<span class="things-to-do__item-sub-text-arrow"></span></div>
                    </div>
                </a>
            </div>

            <div class="things-to-do__item">
                <a href="" class="things-to-do__item-link">
                    <div class="things-to-do__item-image" style="background-image: url('https://i.ytimg.com/vi/n7gcats5uCQ/maxresdefault.jpg')"></div>
                    <div class="things-to-do__item-overlay">
                        <div class="things-to-do__icon --shopping"></div>
                        <div class="things-to-do__item-text">Wine Country</div>
                        <div class="things-to-do__item-sub-text">Suspicious invoices trigger a criminal investigation into a multi-million dollar fraud<span class="things-to-do__item-sub-text-arrow"></span></div>
                    </div>
                </a>
            </div>

          </div>

        </div>

      </div>
    </div>
  </div>
</section>
