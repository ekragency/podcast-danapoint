<section>
  <div class="section">
    <div class="section__container --full-width --no-top-padding">
      <div class="hero">
        <a href="#" class="hero__link">
        <div class="hero__content">
          <img src="<?php echo get_template_directory_uri(); ?>/assets/images/welcome-local-california@2x.png" />
          <div class="button__link --primary --background hero__button">Explore Dana Point</div>
        </div>
        <div class="hero__background js-background-cover background-cover --short">
          <picture>
              <source srcset="" media="(min-width: 1140px)">
                <source srcset="" media="(min-width: 450px)">
                <img srcset="" alt="Hero Image">
            </picture>
        </div>
        <div class="hero__overlay"></div>
        </a>
      </div>
    </div>
  </div>
</section>
