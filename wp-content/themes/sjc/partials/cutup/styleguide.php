<ul class="styleguide__colors">
  <li class="styleguide__color-set">color set 1
    <ul class="styleguide__color-set-items">
          <li class="styleguide__color-swatch">
        <div class="styleguide__color-background" style="background-color: #231f20;"></div>
        <div class="styleguide__color-label">#231f20</div>
      </li>
          <li class="styleguide__color-swatch">
        <div class="styleguide__color-background" style="background-color: #ea423d;"></div>
        <div class="styleguide__color-label">#ea423d</div>
      </li>
          <li class="styleguide__color-swatch">
        <div class="styleguide__color-background" style="background-color: #ffffff;"></div>
        <div class="styleguide__color-label">#ffffff</div>
      </li>
        </ul>
  </li>
  <li class="styleguide__color-set">color set 2
    <ul class="styleguide__color-set-items">
          <li class="styleguide__color-swatch">
        <div class="styleguide__color-background" style="background-color: #42beb6;"></div>
        <div class="styleguide__color-label">#42beb6</div>
      </li>
          <li class="styleguide__color-swatch">
        <div class="styleguide__color-background" style="background-color: #0998b9;"></div>
        <div class="styleguide__color-label">#0998b9</div>
      </li>
          <li class="styleguide__color-swatch">
        <div class="styleguide__color-background" style="background-color: #1f8f9d;"></div>
        <div class="styleguide__color-label">#1f8f9d</div>
      </li>
          <li class="styleguide__color-swatch">
        <div class="styleguide__color-background" style="background-color: #12b1d6;"></div>
        <div class="styleguide__color-label">#12b1d6</div>
      </li>
        </ul>
  </li>
</ul>

<h1>header 1</h1>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl. In id nibh et magna fermentum facilisis non eu purus. Curabitur bibendum gravida neque a sagittis. Morbi mauris metus, elementum at lobortis ut, aliquet eu velit. Cras enim nibh, mattis at massa in, tempus gravida dui. Phasellus vestibulum nisl quis viverra interdum. Vestibulum turpis lacus, consectetur id ipsum nec, blandit tempus nunc. Morbi vel nulla elit. Suspendisse posuere neque dui, et aliquam nibh feugiat ut. Nunc sed nulla diam. Morbi faucibus eleifend nisi, quis vehicula est semper at. Vestibulum molestie nulla non nunc venenatis porttitor. Nulla a neque in velit finibus congue. Etiam at odio sed nunc luctus sollicitudin.</p>
<h2>header 2</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl. In id nibh et magna fermentum facilisis non eu purus. Curabitur bibendum gravida neque a sagittis. Morbi mauris metus, elementum at lobortis ut, aliquet eu velit. Cras enim nibh, mattis at massa in, tempus gravida dui. Phasellus vestibulum nisl quis viverra interdum. Vestibulum turpis lacus, consectetur id ipsum nec, blandit tempus nunc. Morbi vel nulla elit. Suspendisse posuere neque dui, et aliquam nibh feugiat ut. Nunc sed nulla diam. Morbi faucibus eleifend nisi, quis vehicula est semper at. Vestibulum molestie nulla non nunc venenatis porttitor. Nulla a neque in velit finibus congue. Etiam at odio sed nunc luctus sollicitudin.</p>
<h3>header 3</h3>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl. In id nibh et magna fermentum facilisis non eu purus. Curabitur bibendum gravida neque a sagittis. Morbi mauris metus, elementum at lobortis ut, aliquet eu velit. Cras enim nibh, mattis at massa in, tempus gravida dui. Phasellus vestibulum nisl quis viverra interdum. Vestibulum turpis lacus, consectetur id ipsum nec, blandit tempus nunc. Morbi vel nulla elit. Suspendisse posuere neque dui, et aliquam nibh feugiat ut. Nunc sed nulla diam. Morbi faucibus eleifend nisi, quis vehicula est semper at. Vestibulum molestie nulla non nunc venenatis porttitor. Nulla a neque in velit finibus congue. Etiam at odio sed nunc luctus sollicitudin.</p>
<h4>header 4</h4>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl. In id nibh et magna fermentum facilisis non eu purus. Curabitur bibendum gravida neque a sagittis. Morbi mauris metus, elementum at lobortis ut, aliquet eu velit. Cras enim nibh, mattis at massa in, tempus gravida dui. Phasellus vestibulum nisl quis viverra interdum. Vestibulum turpis lacus, consectetur id ipsum nec, blandit tempus nunc. Morbi vel nulla elit. Suspendisse posuere neque dui, et aliquam nibh feugiat ut. Nunc sed nulla diam. Morbi faucibus eleifend nisi, quis vehicula est semper at. Vestibulum molestie nulla non nunc venenatis porttitor. Nulla a neque in velit finibus congue. Etiam at odio sed nunc luctus sollicitudin.</p>
<h5>header 5</h5>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl. In id nibh et magna fermentum facilisis non eu purus. Curabitur bibendum gravida neque a sagittis. Morbi mauris metus, elementum at lobortis ut, aliquet eu velit. Cras enim nibh, mattis at massa in, tempus gravida dui. Phasellus vestibulum nisl quis viverra interdum. Vestibulum turpis lacus, consectetur id ipsum nec, blandit tempus nunc. Morbi vel nulla elit. Suspendisse posuere neque dui, et aliquam nibh feugiat ut. Nunc sed nulla diam. Morbi faucibus eleifend nisi, quis vehicula est semper at. Vestibulum molestie nulla non nunc venenatis porttitor. Nulla a neque in velit finibus congue. Etiam at odio sed nunc luctus sollicitudin.</p>
<h6>header 6</h6>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl. In id nibh et magna fermentum facilisis non eu purus. Curabitur bibendum gravida neque a sagittis. Morbi mauris metus, elementum at lobortis ut, aliquet eu velit. Cras enim nibh, mattis at massa in, tempus gravida dui. Phasellus vestibulum nisl quis viverra interdum. Vestibulum turpis lacus, consectetur id ipsum nec, blandit tempus nunc. Morbi vel nulla elit. Suspendisse posuere neque dui, et aliquam nibh feugiat ut. Nunc sed nulla diam. Morbi faucibus eleifend nisi, quis vehicula est semper at. Vestibulum molestie nulla non nunc venenatis porttitor. Nulla a neque in velit finibus congue. Etiam at odio sed nunc luctus sollicitudin.</p>
<h7>header 7</h7>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl. In id nibh et magna fermentum facilisis non eu purus. Curabitur bibendum gravida neque a sagittis. Morbi mauris metus, elementum at lobortis ut, aliquet eu velit. Cras enim nibh, mattis at massa in, tempus gravida dui. Phasellus vestibulum nisl quis viverra interdum. Vestibulum turpis lacus, consectetur id ipsum nec, blandit tempus nunc. Morbi vel nulla elit. Suspendisse posuere neque dui, et aliquam nibh feugiat ut. Nunc sed nulla diam. Morbi faucibus eleifend nisi, quis vehicula est semper at. Vestibulum molestie nulla non nunc venenatis porttitor. Nulla a neque in velit finibus congue. Etiam at odio sed nunc luctus sollicitudin.</p>

<a href="#">this is a link</a>
<a href="#" class="cta-link">this is a cta link</a>
<em>Italic text</em>
<strong>Bold text</strong>

<div class="wysiwyg">
  <ul>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl.</li>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl.
      <ul>
          <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl.</li>
          <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl.</li>
      </ul>
    </li>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
  </ul>

  <ol>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl.</li>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl.</li>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
        <ol>
          <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl.</li>
          <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl.</li>
        </ol>
    </li>
    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
  </ol>
</div>

<blockquote>
  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vitae bibendum nisl.
</blockquote>

<a href="#" class="button__primary">Button link</a>
<a href="#" class="button__primary --alt">Button link</a>
<a href="#" class="button__secondary">Button link</a>

<div style="padding:40px;background: url('http://lorempixel.com/546/410/');">
  <a href="#" class="button__secondary --alt">Button link</a>
</div>

<div style="padding:40px;background: url('http://lorempixel.com/546/410/');position: relative;">
  <div class="heart">Favorite</div>
</div>

<div style="padding:40px;background: url('http://lorempixel.com/546/410/');position: relative;">
  <div class="heart --pressed">Favorite</div>
</div>

<div style="padding:40px;background: url('http://lorempixel.com/546/410/');position: relative;">
  <div class="heart --alt"></div>
</div>

<div style="padding:40px;background: url('http://lorempixel.com/546/410/');position: relative;">
  <div class="heart --alt --pressed"></div>
</div>

<label for="text">Input text</label><input id="text" type="text">
<label for="textDisabled">Input text Disabled</label><input id="textDisabled" type="text" placeholder="placeholder" disabled>
<label for="textRequired">Input text Required</label><input id="textRequired" type="text" placeholder="placeholder" required>
<label for="textarea">Input textarea</label><textarea name="textarea" id="textarea" cols="30" rows="10"></textarea>

<label for="select">select</label>
<select name="" id="select">
  <option value="1">option 1</option>
  <option value="2">option 2</option>
  <option value="3">option 3</option>
  <option value="4">option 4</option>
  <option value="5">option 5</option>
</select>

<fieldset>
  <input type="radio" name="radios" value="1" id="radio1">
  <label for="radio1">radio1</label>
  <input type="radio" name="radios" value="1" id="radio2">
  <label for="radio2">radio2</label>
  <input type="radio" name="radios" value="1" id="radio3">
  <label for="radio3">radio3</label>
  <input type="radio" name="radios" value="1" id="radio4">
  <label for="radio4">radio4</label>
</fieldset>
<fieldset>
  <input type="checkbox" name="checkes" value="1" id="check1">
  <label for="check1">check1</label>
  <input type="checkbox" name="checkes" value="1" id="check2">
  <label for="check2">check2</label>
  <input type="checkbox" name="checkes" value="1" id="check3">
  <label for="check3">check3</label>
  <input type="checkbox" name="checkes" value="1" id="check4">
  <label for="check4">check4</label>
</fieldset>

<input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
<label for="styled-checkbox-1">Checkbox</label>

<div class="submit__wrapper">
  <input id="submit" type="submit">
</div>