<?php
$map       = get_field('evergreen_map_cta');
$sub_title = $map['evergreen_map_cta_sub_title'];
$title     = $map['evergreen_map_cta_title'];
$link      = $map['evergreen_map_cta_link'];
$image     = $map['evergreen_map_cta_image'];
?>

<section>
  <div class="section">
    <div class="section__container">
        <div class="map-cta">
            <div class="map-cta__container" style="background-image: url('<?php echo $image; ?>');">
                <a href="<?php echo $link; ?>" class="map-cta__link">
                    <div class="map-cta__content">
                        <div class="map-cta__sub-title"><?php echo $sub_title; ?></div>
                        <div class="map-cta__title sub-header"><?php echo $title; ?></div>
                        <div class="button__link map-cta__button">Map</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
  </div>
</section>
