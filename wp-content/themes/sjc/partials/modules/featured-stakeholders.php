<?php
$featured_stakeholders = get_field('evergreen_featured_stakeholders');
$title = $featured_stakeholders['evergreen_featured_stakeholders_title'];
$desktop_image = $featured_stakeholders['evergreen_featured_stakeholders_background_image']['url'];
$mobile_image = $featured_stakeholders['evergreen_featured_stakeholders_background_image_mobile']['url'];
$button_label = $featured_stakeholders['evergreen_featured_stakeholders_button_label'];
$button_link = $featured_stakeholders['evergreen_featured_stakeholders_button_link'];
$stakeholders = $featured_stakeholders['evergreen_featured_stakeholders_list'];
?>

<section>
    <div class="section" style="position: relative;">
        <div class="section__container --full-width --background-image js-background-cover background-cover">

          <picture>
            <source srcset="<?php echo $desktop_image; ?>" media="(min-width: 1140px)">
            <img srcset="<?php echo $mobile_image; ?>" alt="Visit Dana Point">
          </picture>

          <div class="featured-stakeholders__row">

            <?php if(isset($title) && !empty($title)) : ?>
                <div class="featured-stakeholders__title">
                    <h3 class="tertiary-header"><?php echo $title; ?></h3>
                </div>
            <?php endif; ?>

            <?php
            $posts = $stakeholders;
            if( $posts ): ?>
                <div class="featured-stakeholders__container">
                <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                    <?php setup_postdata($post); ?>
                    <?php
                      $term = get_field('stakeholder_primary_things_cat');
                     ?>
                      <div class="featured-stakeholders__card">
                        <a href="<?php the_permalink(); ?>" class="featured-stakeholders__card-link">
                          <div class="featured-stakeholders__card-image js-background-cover background-cover">
                            <img src="<?php echo \NobleStudios\Helpers\getFeaturedImage($post->ID); ?>" alt="<?php echo $post->post_title; ?>">
                          </div>
                        <div class="featured-stakeholders__card-content">
                          <div class="featured-stakeholders__card-header">
                            <h4 class="sub-alt-header"><?php the_title(); ?></h4>
                          </div>
                          <div class="featured-stakeholders__card-category"><?php if ( !empty( $term->name ) ) { echo $term->name; } ?></div>
                        </div>
                        </a>
                      </div>
                    <?php endforeach; ?>
                <?php wp_reset_postdata(); ?>
                </div>
            <?php endif; ?>

            <?php if( isset($button_label) && $button_label ): ?>
            <div class="featured-stakeholders__button --bottom-margin">
              <a href="<?= $button_link; ?>" class="button__link"><?= $button_label; ?></a>
            </div>
            <?php endif; ?>

        </div>

    </div>
</section>
