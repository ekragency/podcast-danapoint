<?php
   extract($partialData);

   if( $video_embed ) {
      if( !empty($placeholder) ) :
        $ph_image = $placeholder['url'];
        $ph_alt   = $placeholder['alt'];
      else :
        $ph_image = '/wp-content/uploads/2018/11/vdp.png';
        $ph_alt   = 'Visit Dana Point';
      endif;
   }
?>
<?php if( isset($paragraph) && !empty($paragraph) ) : ?>
 <section>
   <div class="section --intro-background">
     <div class="section__container">
       <div class="intro-home">
         <div class="intro-home__row">

           <div class="intro-home__column <?php if( $video_embed == false && $image == false) echo '--one-column';?> --text-column">
             <?php if( isset($subtitle) && !empty($subtitle) ) : ?>
                 <div class="intro-home__subheader"><?php echo $subtitle ?></div>
             <?php endif; ?>
             <div class="intro-home__header primary-header">
                  <?php echo $title ?>
             </div>
             <div class="intro-home__body">
                 <?php echo $paragraph ?>
             </div>
             <?php if( $show_button ) :  ?>
                 <div class="intro-home__button">
                    <a href="<?php echo $button_link ?>" class="button__link"><?php echo $button_text ?></a>
                 </div>
             <?php endif; ?>
           </div>

           <!--
           If video is present, it will override the display of the circular image.
           If no video, the image will display and be masked into a circle.
           If no video or image, then the text column will span 100% and be centered.
           -->
           <?php if($video_embed || !empty($image)) : ?>

              <div class="intro-home__column <?php if( $video_embed == false && $image == false) echo '--one-column';?>  --media-column">
                <?php if($video_embed) : ?>
                    <a href="<?php echo $video_id ?>?autoplay=1" class="intro-home__video-link" data-featherlight="iframe" data-featherlight-iframe-width="560" data-featherlight-iframe-height="315" data-featherlight-iframe-border="0" data-featherlight-iframe-allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" data-featherlight-iframe-allowfullscreen="true">
                       <div class="intro-home__video js-background-cover background-cover">
                           <picture>
                              <source srcset="<?php echo $ph_image ?>" media="(min-width: 1140px)">
                              <source srcset="<?php echo $ph_image ?>" media="(min-width: 450px)">
                              <img srcset="<?php echo $ph_image ?>" alt="<?php echo $ph_alt ?>">
                           </picture>
                       </div>
                    </a>

                <?php elseif(!empty($image)) : ?>

                    <div class="intro-home__image js-background-cover background-cover">
                        <picture>
                           <source srcset="<?php echo $image['url'] ?>" media="(min-width: 1140px)">
                           <source srcset="<?php echo $image['url'] ?>" media="(min-width: 450px)">
                           <img srcset="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
                        </picture>
                    </div>

                <?php endif; ?>
             </div>
           <?php endif; ?>
         </div>
       </div>
     </div>
   </div>
 </section>
<?php endif; ?>
