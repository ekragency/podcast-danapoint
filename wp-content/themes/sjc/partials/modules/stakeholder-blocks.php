<?php
  $rowData = $partialData;
  $blockHeader = get_sub_field('sb_block_header');
  $blockSubheader = get_sub_field('sb_block_subheader');
  $mmmSteaks = get_sub_field('sb_blocks');
  $link = get_sub_field('sb_block_link');
  // Hide the separator div if we have the last row
  $hideSeparator = ( $rowData['rowCounter'] == $rowData['maxRows'] ? true : false );
?>

<section>
  <div class="section">
    <div class="section__container">
      <div class="featured">
        <div class="featured__header">
          <?php if( isset($blockHeader) && !empty($blockHeader) ): ?>
            <h2 class="secondary-header"> <?= $blockHeader; ?> </h2>
          <?php endif; ?>
          <?php if( isset($blockSubheader) && !empty($blockSubheader) ): ?>
            <p> <?= $blockSubheader; ?> </p>
          <?php endif; ?>
        </div>
        <div class="featured__button">
          <?php if( isset($link['title']) && !empty($link['title']) ): ?>
            <a href="<?= $link['url']; ?>" class="button__link"> <?= $link['title']; ?> </a>
          <?php endif; ?>
        </div>
        <div class="featured__blocks js-slider-mobile">
          <?php if( $mmmSteaks ): foreach( $mmmSteaks as $theSteak ): ?>
            <?php $steakType = get_field( 'stakeholder_type', $theSteak ); ?>
            <div class="image-block --4-columns">
              <a href="<?= get_permalink($theSteak); ?>" class="image-block__container">
                <div class="image-block__content">
                  <div class="image-block__image js-background-cover background-cover">
                    <img src="<?php echo \NobleStudios\StakeholderHelper\getFirstStakeHolderSlide($theSteak, $steakType); ?>" alt="<?= get_the_title($theSteak); ?>">
                  </div>
                  <div class="image-block__header">
                    <h3 class="sub-header"> <?= get_the_title($theSteak); ?> </h3>
                  </div>
                </div>
              </a>
            </div>
          <?php endforeach; endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
