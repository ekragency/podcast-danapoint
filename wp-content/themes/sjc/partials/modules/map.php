<?php
  // Before you resides the GLORIOUS interactive map
  // Categories to map are assigned per need basis.
  // We are filtering by categories and meta key/value pairs.
  // There is a data attribute to identify which one we need.
  $typeMapping = \NobleStudios\Utilities\Configure::read('stakeholderTypeMapping');
?>

<section>
  <div class="section">
    <div class="section__container">
      <div class="interactive-map" id="interactive-map">
        <div class="interactive-map__overlay js-map-overlay"></div>
        <div class="js-map-modal-container"> <?php \NobleStudios\Utilities\Tools::renderPartial("js-templates/map-modal"); ?> </div>
        <div id="google-map" class="interactive-map__container"></div>
        <div class="interactive-map__sidebar">
          <div class="map-sidebar">
            <ul class="map-sidebar__items js-map-sidebar --expanded">
                <li class="map-sidebar__item">
                  <a class="map-sidebar__button js-map-sidebar-button js-map-meta"
                    data-key="stakeholder_type" data-value="Place to Stay"
                    data-icon="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/marker-stay.svg">

                    <span class="map-sidebar__icon --turquoise">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/hotel.svg" alt="" class="js-svg-swap">
                    </span>
                    <span class="map-sidebar__text">Places to Stay</span>
                  </a>
                </li>
                <li class="map-sidebar__item">
                    <a class="map-sidebar__button js-map-sidebar-button js-map-category"
                      data-category="rv-campgrounds"
                      data-taxonomy="<?php echo $typeMapping['Place to Stay']; ?>"
                      data-icon="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/marker-camping.svg">

                      <span class="map-sidebar__icon --brown">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/tent.svg" alt="" class="js-svg-swap">
                      </span>
                      <span class="map-sidebar__text">Camping</span>
                    </a>
                </li>
                <li class="map-sidebar__item">
                  <a class="map-sidebar__button js-map-sidebar-button js-map-meta"
                    data-key="stakeholder_type" data-value="Dining"
                    data-icon="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/marker-dine.svg">

                    <span class="map-sidebar__icon --orange --active">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/dine.svg" alt="" class="js-svg-swap">
                    </span><span class="map-sidebar__text">Dining</span>
                  </a>
                </li>
                <li class="map-sidebar__item">
                  <a class="map-sidebar__button js-map-sidebar-button js-map-category"
                    data-category="attractions"
                    data-taxonomy="<?php echo $typeMapping['Things to Do']; ?>"
                    data-icon="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/marker-attraction.svg">

                    <span class="map-sidebar__icon --yellow">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/star.svg" alt="" class="js-svg-swap">
                    </span>
                    <span class="map-sidebar__text">Attractions</span>
                  </a>
                </li>
                <li class="map-sidebar__item">
                  <a class="map-sidebar__button js-map-sidebar-button js-map-category"
                    data-category="hiking"
                    data-taxonomy="<?php echo $typeMapping['Things to Do']; ?>"
                    data-icon="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/marker-hike.svg">

                    <span class="map-sidebar__icon --green">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/hike.svg" alt="" class="js-svg-swap">
                    </span>
                    <span class="map-sidebar__text">Hiking</span>
                  </a>
                </li>
                <li class="map-sidebar__item">
                  <a class="map-sidebar__button js-map-sidebar-button js-map-category"
                    data-category="biking"
                    data-taxonomy="<?php echo $typeMapping['Things to Do']; ?>"
                    data-icon="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/marker-biking.svg">

                    <span class="map-sidebar__icon --red">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/bike.svg" alt="" class="js-svg-swap">
                    </span>
                    <span class="map-sidebar__text">Biking</span>
                  </a>
                </li>
                <li class="map-sidebar__item">
                  <a class="map-sidebar__button js-map-sidebar-button js-map-category"
                    data-category="watersports"
                    data-taxonomy="<?php echo $typeMapping['Things to Do']; ?>"
                    data-icon="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/marker-kayak.svg">

                    <span class="map-sidebar__icon --blue">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/kayak.svg" alt="" class="js-svg-swap">
                    </span>
                    <span class="map-sidebar__text">Watersports</span>
                  </a>
                </li>
                <li class="map-sidebar__item">
                  <a class="map-sidebar__button js-map-sidebar-button js-map-category"
                    data-category="fishing"
                    data-taxonomy="<?php echo $typeMapping['Things to Do']; ?>"
                    data-icon="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/marker-fish.svg">

                    <span class="map-sidebar__icon --purple">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/fish.svg" alt="" class="js-svg-swap">
                    </span>
                    <span class="map-sidebar__text">Fishing</span>
                  </a>
                </li>
                <li class="map-sidebar__item">
                  <a class="map-sidebar__button js-map-sidebar-button js-map-category"
                    data-category="winter-activities"
                    data-taxonomy="<?php echo $typeMapping['Things to Do'] ?>"
                    data-icon="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/marker-wintersports.svg">

                    <span class="map-sidebar__icon --light-blue">
                      <img src="<?php echo get_template_directory_uri(); ?>/assets/visual/svgs/min/snowflake.svg" alt="" class="js-svg-swap">
                    </span>
                    <span class="map-sidebar__text">Winter Activities</span>
                  </a>
                </li>
            </ul>
            <a class="map-sidebar__toggle js-map-sidebar-toggle"></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
