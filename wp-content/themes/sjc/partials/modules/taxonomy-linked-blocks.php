<?php
  $blockHeader = get_sub_field('td_block_header');
  $blocks = get_sub_field('td_blocks');
?>
<section>
  <div class="section">
    <div class="section__container">
      <div class="featured">

        <?php if( !empty($blockHeader ) ): ?>
        <div class="featured__header">
          <h2 class="secondary-header"> <?php echo $blockHeader; ?> </h2>
        </div>
        <?php endif; ?>

        <div class="featured__blocks">
          <?php if( $blocks ): foreach( $blocks as $thingToDo ): ?>
            <div class="image-block">
              <a href=" <?php echo get_term_link($thingToDo->term_id, $thingToDo->taxonomy); ?> " class="image-block__container">
                <div class="image-block__content">
                  <div class="image-block__image js-background-cover background-cover">
                    <img src="<?php echo get_field('thing_to_do_category_thumb', $thingToDo->term_id) ?>" alt="<?php echo $thingToDo->name; ?>">
                  </div>
                  <div class="image-block__header">
                    <h3 class="sub-header"> <?php echo $thingToDo->name; ?> </h3>
                  </div>
                </div>
              </a>
            </div>
          <?php endforeach; endif; ?>
        </div>

        <div class="featured__button">
          <a href="<?php echo esc_url(home_url('/things-to-do')); ?>" class="button__link"> All things to do </a>
        </div>
        <div class="section__divider"></div>
      </div>
    </div>
  </div>
</section>
