<?php
  $id = get_the_ID();
  $sidebar = \NobleStudios\SideBarBuilder\buildPageSidebar($id);
  $ancestors = get_ancestors( $id, 'page' );
  $toplevel = end($ancestors);

  if( get_field('sidebar_images', 'option') ) {
    $images = get_field('sidebar_images', 'option');
  }
?>

<aside class="page-sidebar__aside">
  <div class="page-sidebar">
    <div class="page-sidebar__container">
      <div class="page-sidebar__header">
        <?php if ($toplevel == False): ?>
            <h6><?php echo get_the_title($toplevel); ?></h6>
        <?php else: ?>
            <h6><a href="<?php echo get_the_permalink($toplevel); ?>"><?php echo get_the_title($toplevel); ?></a></h6>
        <?php endif; ?>
      </div>
      <ul class="page-sidebar__items">
      <?php foreach ($sidebar as $item) : ?>
          <li class="page-sidebar__item">
          <?php if($item->parent !== 0) : ?>
            <a href="<?php echo $item->link; ?>"><?php echo $item->name; ?></a>
          <?php else: ?>
            <?php if ($toplevel != False): ?>
              <button class="page-sidebar__button --active"><?php echo $item->name; ?></button>
            <?php endif; ?>
            <?php if(isset($item->children) && !empty($item->children)) : ?>
              <?php foreach ($item->children as $subChild) : ?>
                <li class="page-sidebar__item"><a href="<?php echo $subChild->link; ?>"><?php echo $subChild->name; ?></a></li>
              <?php endforeach; ?>
          <?php  endif; ?>
          <?php endif; ?>
          </li>
        <?php endforeach; ?>
      </ul>
      <div class="page-sidebar__image-links">
        <?php if(isset($images) && !empty($images)) :
          foreach ($images as $image) :
        ?>
        <div class="page-sidebar__image-link">
          <a href="<?php echo $image['sidebar_image_link'];?>" <?php if($image['sidebar_external_link']) { echo 'target="_blank"'; } ?>>
            <img src="<?php echo $image['sidebar_image'];?>" alt="<?php echo $image['sidebar_image_link'];?>">
          </a>
        </div>
        <?php
          endforeach;
        endif;
        ?>
      </div>

    </div>
  </div>
</aside>
