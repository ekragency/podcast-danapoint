<?php
if( have_rows('evergreen_featured_blog_posts_group') ):
  while( have_rows('evergreen_featured_blog_posts_group') ): the_row();
    if( have_rows('evergreen_featured_blog_posts') ):
      while( have_rows('evergreen_featured_blog_posts') ): the_row();

      $title = get_sub_field('title');
      $button_label = get_sub_field('button_label');
      $button_link = get_sub_field('button_link');

?>
<section>
  <div class="section">
    <div class="section__container">
      <div class="featured">

        <?php if( isset($title) && !empty($title) ) :  ?>
        <div class="featured__header">
          <h3><?php echo $title; ?></h3>
        </div>
        <?php endif; ?>

        <div class="featured__blocks">
        <?php
        $posts = get_sub_field('posts');
        if( $posts ): ?>
            <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                <?php setup_postdata($post); ?>
                  <div class="excerpt-block">
                    <a href="<?php the_permalink(); ?>" class="excerpt-block__link">
                      <div class="excerpt-block__container">
                        <div class="excerpt-block__content">
                          <div class="excerpt-block__image js-background-cover background-cover">
                            <img src="<?php echo \NobleStudios\Helpers\getFeaturedImage($post->ID); ?>" alt="<?php echo $post->post_title; ?>">
                          </div>
                          <div class="excerpt-block__header">
                            <h3 class="sub-alt-header"><?php the_title(); ?></h3>
                          </div>
                          <div class="excerpt-block__excerpt">

                            <?php echo apply_filters('the_excerpt', get_post_field('post_excerpt', $post->ID)); ?>

                          </div>
                        </div>
                      </div>
                    </a>
                  </div>
                <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>

        </div>
        <?php if( isset($button_label) && !empty($button_label) ): ?>
        <div class="featured__button --bottom-margin">
          <a href="<?php echo $button_link; ?>" class="button__link"><?php echo $button_label; ?></a>
        </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
<?php
      endwhile;
    endif;
  endwhile;
endif; ?>
