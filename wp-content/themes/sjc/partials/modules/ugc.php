<?php

    $galleryID = get_sub_field('gallery_id');

    if (empty($galleryID)) {
        $galleryID = '9813c984';
    }
 ?>
<script id="cr__init-<?= $galleryID ?>" src="https://embed.crowdriff.com/js/init?hash=<?= $galleryID ?>" async></script>
