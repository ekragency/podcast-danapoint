<?php
$terms = get_terms('category', array(
    'hide_empty'       => 0,
    'orderby'          => 'name',
    'number'           => 5,
) );

?>

<aside>
  <div class="blog-sidebar">
    <div class="blog-sidebar__container">

      <div class="blog-sidebar__header"><h5>Categories</h5></div>
      <ul class="blog-sidebar__items">
      <?php
        foreach ($terms as $term) {
          echo '<li class="blog-sidebar__item"><a href="'.get_term_link($term->term_id).'">'.$term->name.'</a></li>';
        }
      ?>
      </ul>
    </div>
  </div>
</aside>
