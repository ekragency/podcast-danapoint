<?php
    $content = get_sub_field('op_content');
    $full_width = get_sub_field('full_width');
?>
<section>
  <div class="section <?= $full_width == true ? '--full-width' : null ?>">
    <div class="section__container">
      <div class="open-content">
        <?= $content; ?>
      </div>
    </div>
  </div>
</section>
