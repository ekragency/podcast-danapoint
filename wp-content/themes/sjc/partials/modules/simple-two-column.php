<?php
  $subHeader    = get_sub_field('stc_header');
  $paragraph    = get_sub_field('stc_paragraph');
  $link         = get_sub_field('stc_link');
  $image        = get_sub_field('stc_featured_image');
  $reverseCol   = get_sub_field('stc_reverse_columns');
?>
<section>
  <div class="section">
    <div class="section__container --full-width --big-padding">
      <div class="simple-content-block <?= $reverseCol ? '--reversed' : ''; ?>">
        <div class="simple-content-block__medium">
            <?php if( isset($subHeader) && !empty($subHeader) ): ?>
                <div class="simple-content-block__header">
                    <h2> <?= $subHeader; ?> </h2>
                </div>
            <?php endif; ?>
            <?php if( isset($paragraph) && !empty($paragraph) ): ?>
              <div class="simple-content-block__body">
                 <?php echo $paragraph; ?>
              </div>
            <?php endif; ?>
            <?php if( isset($link['text']) && !empty( $link['text'] ) ): ?>
              <div class="simple-content-block__link">
                <a href="<?= $link['url']; ?>" class="button__link" <?= ($link['target'] ? 'target="_blank"' : '' ); ?>>
                  <?= $linkText; ?>
                </a>
              </div>
            <?php endif; ?>
          </div>
          <div class="simple-content-block__big">
            <?php if( isset($image['url']) && !empty( $image['url'] ) ): ?>
              <img src="<?= $image['url']; ?>" alt="<?= $image['alt']; ?>">
            <?php endif; ?>
          </div>
      </div>
    </div>
  </div>
</section>
