<?php

    use function NobleStudios\Helpers\getFeaturedImage;

    $title = get_sub_field('title');
    $subtitle = get_sub_field('description');
    $button_link = get_sub_field('button_link');
    $button_text = get_sub_field('button_text');
    $automagic = get_sub_field('automagic');
    $mobile = get_sub_field('background_mobile');
    $tablet = get_sub_field('background_tablet');
    $desktop = get_sub_field('background_desktop');

    $posts = array();

    if ($automagic) {
        $type = get_sub_field('type');
        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 4,
            'offset' => 0,
            'tax_query' => array(),
        );

        $terms = array();
        if ($type == 'category') {
            $terms = get_sub_field('category');
            $maybeTerms = array();
            foreach ($terms as $term) {
                array_push($maybeTerms, $term->slug);
            }

            array_push($args['tax_query'], array(
                array(
                    'taxonomy' => 'category',
                    'field'    => 'slug',
                    'terms'    => $maybeTerms,
                )
            ));
        } elseif ($type == 'tag') {
            $terms = get_sub_field('tag');
            $maybeTerms = array();

            foreach ($terms as $term) {
                array_push($maybeTerms, $term->slug);
            }

            array_push($args['tax_query'], array(
                array(
                    'taxonomy' => 'post_tag',
                    'field'    => 'slug',
                    'terms'    => $maybeTerms,
                )
            ));
        }

        $maybePosts = new \WP_Query($args);

        if ($maybePosts->have_posts()) {
            while ($maybePosts->have_posts()) {
                $maybePosts->the_post();
                array_push($posts, get_post());
            }
        }
    } else {
        $maybePosts = get_sub_field('posts');
        if (!empty($maybePosts) && $maybePosts != false) {
            foreach ($maybePosts as $maybePost) {
                array_push($posts, $maybePost['post']);
            }
        }
    }

 ?>
<section>
  <div class="section js-background-cover background-cover">
    <div class="section__container">

      <picture>
        <?php if( !empty($desktop) ) : ?>
          <source srcset="<?php echo $desktop; ?>" media="(min-width: 1140px)">
        <?php endif; ?>
        <?php if( !empty($tablet) ) : ?>
          <source srcset="<?php echo $tablet; ?>" media="(min-width: 450px)">
        <?php endif; ?>
        <?php if( !empty($mobile) ) : ?>
          <img srcset="<?php echo $mobile; ?>" alt="Hero Image">
        <?php endif; ?>
      </picture>

      <div class="featured-articles">

        <?php if ( !empty($title) ): ?>
          <div class="featured-articles__header secondary-header"><?php echo $title ?></div>
        <?php endif; ?>

        <?php if ( !empty($subtitle) ): ?>
          <div class="featured-articles__description"><?php echo $subtitle ?></div>
        <?php endif; ?>

        <?php if ( !empty($button_link) ): ?>
          <div class="featured-articles__button">
            <a href="<?php echo $button_link['url'] ?>" class="button__link"><?php echo $button_text ?></a>
          </div>
        <?php endif; ?>


        <div class="featured__blocks js-slider-mobile">

            <?php
                //The max of 4 is defined in ACF, so we don't have to check against it here.
                $columns = count($posts);
                $counter = 1;
                foreach ($posts as $post): ?>
                <?php
                  $counter++;
                  $custom_title = get_field('featured_article_title', $post->ID);
                  $custom_description = get_field('featured_article_description', $post->ID);
                ?>
                <div class="image-block gm-block-<?php echo $counter; ?> <?php echo $columns ?>">
                  <a href="<?php echo get_permalink($post) ?>" class="image-block__container">
                    <div class="image-block__content">
                      <div class="image-block__image <?php echo $columns ?> js-background-cover background-cover">
                        <img src="<?php echo getFeaturedImage($post->ID) ?>" alt="<?php echo wp_trim_words($post->post_title, 7, '...'); ?>">
                      </div>
                      <div class="image-block__header">
                        <h3 class="sub-header"> <?php  echo wp_trim_words($custom_title, 7, '...'); ?> </h3>
                      </div>
                      <div class="image-block__description">
                        <?php  echo wp_trim_words($custom_description, 25, '...'); ?> 
                      </div>

                    </div>
                  </a>
                </div>

            <?php endforeach; ?>

        </div>
      </div>

    </div>
  </div>
</section>



<?php
wp_reset_postdata();
wp_reset_query();
