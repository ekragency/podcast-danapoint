<?php
  $blog = false;
  if(isset($partialData)) {
    $blog = $partialData;
  }
?>
<section>
  <div class="section">
    <div class="section__container --short">
      <div class="page-body">
        <div class="page-body__content <?php if($blog) { echo '--blog'; } ?>">
          <div class="wizzywig">
            <?php if( have_posts() ): while ( have_posts() ): ?>
            	<?php the_post(); ?>
              <?php the_content(); ?>
            <?php endwhile; endif; ?>
          </div>
        </div>
        <?php if(!$blog): ?>
          <div class="page-body__sidebar">
            <?php \NobleStudios\Utilities\Tools::renderPartial("modules/page-sidebar");?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
