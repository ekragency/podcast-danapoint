<?php
$gallery_teaser = get_field('evergreen_gallery_teaser');
$title      = $gallery_teaser['evergreen_gallery_teaser_title'];
$intro      = $gallery_teaser['evergreen_gallery_teaser_intro'];
$images     = $gallery_teaser['evergreen_gallery_teaser_images'];
$paragraph  = $gallery_teaser['evergreen_gallery_teaser_paragraph'];
$credit     = $gallery_teaser['evergreen_gallery_teaser_images_credit'];
?>

<?php if(isset($title) && !empty($title)) : ?>

  <div class="section">
      <div class="section__container">
        <div class="gallery-teaser">

          <div class="gallery-teaser__row">
            <?php if(isset($title)) : ?>
              <h2 class="gallery-teaser__title"><?= $title; ?></h2>
            <?php endif; ?>

            <?php if(isset($intro)) : ?>
              <div class="gallery-teaser__intro"><?= $intro; ?></div>
            <?php endif; ?>
          </div>

          <div class="gallery-teaser__row">
              <div class="gallery-teaser__column">
                <div class="gallery-teaser__grid <?php if($images && count($images) <= 1): echo "--full-width"; endif; ?>">
                <?php foreach( $images as $image_array ): ?>
                  <?php if(count($images) > 1): ?>
                      <?php foreach( $image_array as $image ): ?>
                        <img src="<?= $image["url"]; ?>" alt="<?= $image["alt"]; ?>">
                      <?php endforeach; ?>
                    <?php else: ?>
                      <?php foreach( $image_array as $image ): ?>
                        <img src="<?= $image["url"]; ?>" alt="<?= $image["alt"]; ?>">
                      <?php endforeach; ?>
                    <?php endif; ?>
                <?php endforeach; ?>
                </div>

                <?php if(isset($credit)) : ?>
                  <div class="gallery-teaser__credit"><em><?= $credit; ?></em></div>
                <?php endif; ?>
              </div>

              <div class="gallery-teaser__column">
                <?php if(isset($paragraph)) : ?>
                    <?= $paragraph; ?>
                <?php endif; ?>
              </div>
            </div>

        </div>
      </div>
    </div>

<?php endif; ?>
