<?php
  $top = '';
  $title = get_the_title();
  $body = '';
  $classes = array();
  $noHero = false; // If we don't have a header, we need more margin above the intro

  if( isset($partialData) ) {
    if( isset($partialData['title']) && !empty($partialData['title']) ) {
      $title = $partialData['title'];
    }
    if( isset($partialData['top']) && !empty($partialData['top']) ) {
      $top = $partialData['top'];
    }
    if( isset($partialData['body']) && !empty($partialData['body']) ) {
      $body = $partialData['body'];
    }
    if( isset($partialData['classes']) && !empty($partialData['classes']) ) {
      $classes = $partialData['classes'];
    }
    if( isset($partialData['noHero']) && $partialData['noHero'] ) {
      $noHero = $partialData['noHero'];
    }
  }
?>
<section>
  <div class="section">
    <div class="section__container <?= $noHero ? ' --no-hero ' :  ''; ?> --no-bottom-padding">
      <div class="intro">

        <?php if( !empty($top) ) : ?>
        <div class="intro__top-header">
          <h2 class="sub-alt-header"><?= $top; ?></h2>
        </div>
        <?php endif; ?>

        <?php if( !empty($title) ) : ?>
        <div class="intro__header <?php if(!isset($body) || empty($body)) { echo '--no-margin'; } ?>">
          <h1><?= $title; ?></h1>
        </div>
        <?php endif; ?>

        <?php if( is_single() ): ?>
          <div class="intro__blog-meta">
            <span><?= get_the_date(); ?></span>
          </div>
        <?php else: ?>
          <div class="intro__body <?php echo isset($classes['body']) ? $classes['body'] : ''; ?>">
            <?= $body; ?>
          </div>
        <?php endif; ?>
      </div>
    </div>
  </div>
</section>
