<?php
    $title = get_sub_field('title');
    $description = get_sub_field('description');
    $blocks = get_sub_field('blocks');
    $extendo = $blocks[0];
    array_shift($blocks);
 ?>
<section>
  <div class="section">
    <div class="section__container --full-width --no-top-padding --no-bottom-padding">
      <div class="things-to-do">

        <?php if (!empty($title) || !empty($description)): ?>
            <div class="things-to-do__header section__container">
              <?php if (!empty($title)): ?>
                  <div class="things-to-do__heading secondary-header"><?= $title ?></div>
              <?php endif; ?>
              <?php if (!empty($description)): ?>
                  <div class="things-to-do__heading-description">
                      <p><?= $description ?></p>
                  </div>
              <?php endif; ?>

            </div>
        <?php endif; ?>


        <div class="things-to-do__items">

          <div class="things-to-do__column">

            <div class="things-to-do__item --extendo js-background-cover background-cover">
                <picture>
                  <source srcset="<?= $extendo['background_image'] ?>" media="(min-width: 1140px)">
                  <img srcset="<?= $extendo['background_image'] ?>" alt="Hero Image">
                </picture>
                <a href="<?= $extendo['link'] ?>" target="_self" class="things-to-do__item-link">
                    <div class="things-to-do__item-text">
                        <h4><?= $extendo['title'] ?></h4>
                    </div>
                </a>
            </div>

          </div>

          <div class="things-to-do__column">
              <?php foreach ($blocks as $block): ?>
                  <div class="things-to-do__item --half-height js-background-cover background-cover">
                      <picture>
                        <source srcset="<?= $block['background_image'] ?>" media="(min-width: 1140px)">
                        <img srcset="<?= $block['background_image'] ?>" alt="Hero Image">
                      </picture>
                      <a href="<?= $block['link'] ?>" target="_self" class="things-to-do__item-link">
                        <div class="things-to-do__item-text">
                          <h4><?= $block['title'] ?></h4>
                        </div>
                      </a>
                  </div>
              <?php endforeach; ?>
          </div>

        </div>

      </div>
    </div>
  </div>
</section>
