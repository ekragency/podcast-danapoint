<?php
  $p1_title       = get_sub_field('sp_p1_title');
  $p1_image       = get_sub_field('sp_p1_image');
  $p1_description = get_sub_field('sp_p1_description');
  $p1_cta         = get_sub_field('sp_p1_cta');

  $p2_title       = get_sub_field('sp_p2_title');
  $p2_image       = get_sub_field('sp_p2_image');
  $p2_description = get_sub_field('sp_p2_description');
  $p2_cta         = get_sub_field('sp_p2_cta');
?>
<section>
  <div class="section">
    <div class="section__container">
      <div class="split-post__container">
        <div class="split-post">
          <div class="image-block__image js-background-cover background-cover">
            <img src="<?= $p1_image['url']; ?>" alt="<?= $p1_image['alt']; ?>">
          </div>
          <h4><?= $p1_title; ?></h4>
          <p><?= $p1_description; ?></p>
          <a href="<?= $p1_cta['url']; ?>" class="button__link" target="<?= ($p1_cta['target'] ? 'target="_blank"' : '' ); ?>"><?= $p1_cta['title']; ?></a>
        </div>
        <div class="split-post">
          <div class="image-block__image js-background-cover background-cover">
            <img src="<?= $p2_image['url']; ?>" alt="<?= $p2_image['alt']; ?>">
          </div>
          <h4><?= $p2_title; ?></h4>
          <p><?= $p2_description; ?></p>
          <a href="<?= $p2_cta['url']; ?>" class="button__link" target="<?= ($p2_cta['target'] ? 'target="_blank"' : '' ); ?>"><?= $p2_cta['title']; ?></a>
        </div>
      </div>
    </div>
  </div>
</section>
