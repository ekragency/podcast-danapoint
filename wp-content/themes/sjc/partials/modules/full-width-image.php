<?php
    $mobile = get_sub_field('mobile_image');
    $tablet = get_sub_field('tablet_image');
    $desktop = get_sub_field('desktop_image');
    $is_map = get_sub_field('is_map');
    $has_map = '';

    if($is_map) $has_map = '--map';
?>

  <section>
    <div class="section">
      <div class="section__container --full-width --no-top-padding --no-bottom-padding">
        <div class="full-width-image js-background-cover background-cover <?php echo $has_map; ?>">
          <picture>
            <?php if( !empty($desktop) ) :  ?>
                <source srcset="<?php echo $desktop['url'] ?>" media="(min-width: 1140px)">
            <?php endif; ?>
            <?php if( !empty($tablet) ) :  ?>
                <source srcset="<?php echo $tablet['url'] ?>" media="(min-width: 768px)">
            <?php else : ?>
                <source srcset="<?php echo $mobile['url'] ?>" media="(min-width: 450px)">
            <?php endif; ?>
            <?php if( !empty($mobile) ) :  ?>
                <img srcset="<?php echo $mobile['url'] ?>" alt="<?php echo $mobile['alt'] ?>">
            <?php endif; ?>
          </picture>
        </div>
      </div>
    </div>
  </section>
