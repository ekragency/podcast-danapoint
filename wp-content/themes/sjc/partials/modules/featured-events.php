<?php
    use function NobleStudios\Helpers\getFeaturedImage;

    $title = get_sub_field('title');
    $description = get_sub_field('description');
    $automagic = get_sub_field('automagic');
    $link = get_sub_field('link');
    $events = array();

    if ($automagic) {
        $events = tribe_get_events(
            array(
                'start_date' => date( 'Y-m-d H:i:s' ),
                'posts_per_page' => 3,
                'tax_query'=> array(
            				array(
            					'taxonomy' => 'tribe_events_cat',
            					'field' => 'slug',
            					'terms' => 'signature-events'
            				)
            			)
                )
        );
    } else {
        $maybeEvents = get_sub_field('events');

        if (!empty($maybeEvents) && $maybeEvents != false) {
            foreach ($maybeEvents as $maybeEvent) {
                array_push($events, $maybeEvent['event']);
            }
        }

    }

    if (empty($events)) return;
 ?>
<section>
  <div class="section">
    <div class="section__container">
      <div class="featured-events">
        <?php if ( isset($title) && !empty($title)): ?>
            <div class="featured-events__header secondary-header"><?php echo $title ?></div>
        <?php endif; ?>
        <?php if (isset($description) && !empty($description)): ?>
            <p class="featured-events__description"><?php echo $description ?></p>
        <?php endif; ?>

        <?php if ($automagic): ?>
            <a href="<?php echo esc_url( tribe_get_events_link() ); ?>" class="button__link featured-events__button">View All Events</a>
        <?php elseif (isset($link['url']) && !empty($link['url'])): ?>
            <a href="<?php echo $link['url'] ?>" target="<?php echo $link['target'] ?>" class="button__link featured-events__button"><?php echo $link['title'] ?? 'View All Events' ?></a>
        <?php endif; ?>

        <div class="section__divider"></div>

        
        <div class="featured__blocks js-slider-mobile">

            <?php
                //The max of 4 is defined in ACF, so we don't have to check against it here.
                $columns = count($events);
                foreach ($events as $post): ?>
                <?php
                  $custom_title = get_field('featured_article_title', $post->ID);
                  $custom_description = get_field('featured_article_description', $post->ID);
                ?>
                <div class="image-block <?php echo $columns ?>">
                  <a href="<?php echo get_permalink($post) ?>" class="image-block__container">
                    <div class="image-block__content">
                      <div class="image-block__image <?php echo $columns ?> js-background-cover background-cover">
                        <img src="<?php echo getFeaturedImage($post->ID) ?>" alt="<?php echo wp_trim_words($post->post_title, 7, '...'); ?>">

                      </div>
                      <div class="image-block__header">
                        <div class="image-block__header-date">
                          <?php if (tribe_event_is_multiday($post)): ?>
                              <?php echo tribe_get_start_date($post, false, 'M j, Y'); ?> - <?php echo tribe_get_end_date($post, false, 'M j, Y');?>
                          <?php else: ?>
                              <?php echo tribe_get_start_date($post, false, 'M j, Y'); ?>
                          <?php endif; ?>
                        </div>
                        <h3 class="sub-header"> <?php  echo wp_trim_words($post->post_title, 7, '...'); ?> </h3>
                      </div>
                    </div>
                  </a>
                </div>

            <?php endforeach; ?>

        </div>

      </div>

    </div>
  </div>
</section>
<?php
wp_reset_postdata();
wp_reset_query(); ?>