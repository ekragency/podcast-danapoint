<?php

    $title = get_sub_field('title');
    $description = get_sub_field('description');
    $link = get_sub_field('link');

 ?>

  <div class="section full-width-image-content">
    <div class="full-width-image-content__mobile-image"></div>
    <div class="section__container">
            <div class="full-width-image-content__container">
                <div class="full-width-image-content__content">
                    <?php if (!empty($title)): ?>
                        <div class="full-width-image-content__title tertiary-header"><?= $title ?></div>
                    <?php endif; ?>
                    <?php if (!empty($description)): ?>
                        <div class="full-width-image-content__description">
                            <?= $description ?>
                        </div>
                    <?php endif; ?>
                    <?php if (isset($link['url']) && !empty($link['url'])): ?>
                        <a href="<?= $link['url'] ?>" target="<?= $link['target'] ?>" class="button__button"><?= $link['title'] ?></a>
                    <?php endif; ?>

                </div>
            </div>
    </div>
</div>
