<?php
    use function NobleStudios\Helpers\getFeaturedImage;

    $signature_events = array();
    $signature_events = tribe_get_events(
      array(
          'start_date' => date( 'Y-m-d H:i:s' ),
          'posts_per_page' => 3,
          'tax_query'=> array(
  				array(
  					'taxonomy' => 'tribe_events_cat',
  					'field' => 'slug',
  					'terms' => 'signature-events'
  				)
  			)
     )
    );

    if (empty($signature_events)) return;

 ?>
<section>
  <div class="section">
    <div class="section__container">
      <div class="featured-events-row">
        <div class="featured-events-row__header secondary-header">Signature Events</div>

        <div class="section__divider"></div>

        <div class="cards js-slider-mobile --alt">

            <?php foreach ($signature_events as $signature_event): ?>
                <div class="cards__card">
                  <a href="<?php echo get_permalink($signature_event) ?>" class="cards__card-link">
                    <div class="cards__card-image" style="background-image: url('<?php echo getFeaturedImage($signature_event->ID) ?>');"></div>
                    <div class="cards__card-title">
                        <div class="cards__card-date">
                            <?php if (tribe_event_is_multiday($signature_event)): ?>
                                <?php echo tribe_get_start_date($signature_event, false, 'M j, Y'); ?> - <?php echo tribe_get_end_date($signature_event, false, 'M j, Y');?>
                            <?php else: ?>
                                <?php echo tribe_get_start_date($signature_event, false, 'M j, Y'); ?>
                            <?php endif; ?>
                        </div>
                        <?php echo $signature_event->post_title ?>
                    </div>
                  </a>
                </div>
            <?php endforeach; ?>

        </div>
      </div>
    </div>
  </div>
</section>
