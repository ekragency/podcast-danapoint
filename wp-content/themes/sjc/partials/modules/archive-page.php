<section>
  <div class="section">
    <div class="section__container">
      <div class="archive-page">
        <div class="archive-page__count-container">
          <div class="archive-page__count">
            <?php if( is_date() ): ?>
              <h1> Posted in <?php echo preg_replace('/^,\s/', '', single_month_title(', ', false)); ?> </h1>
            <?php elseif( is_author() ): ?>
              <h1> Posts by <?php echo ucfirst(get_query_var('author_name')); ?> </h1>
            <?php elseif( is_category() ): ?>
              <h1> Posts in <?php echo single_cat_title(); ?> </h1>
            <?php else: ?>
              <h1> <?php echo get_the_title( get_option('page_for_posts') ); ?> </h1>
            <?php endif; ?>
          </div>
        </div>
        <div class="archive-page__content">
          <?php \NobleStudios\Utilities\Tools::renderPartial("modules/archive-results");?>
          <?php \NobleStudios\Utilities\Tools::renderPartial("modules/archive-pagination");?>
        </div>
      </div>
    </div>
  </div>
</section>
