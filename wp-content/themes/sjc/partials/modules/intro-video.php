<?php
$video_intro = get_field('evergreen_video_intro');

$title      = $video_intro['evergreen_video_intro_title'];
$subheader = $video_intro['evergreen_video_intro_sub_title'];
$paragraph  = $video_intro['evergreen_video_intro_description'];
$video      = $video_intro['evergreen_video_intro_video'];
?>


<?php if (isset($title) && !empty($title)) : ?>

  <div class="section">
      <div class="section__container">
        <div class="intro --overlap --transparent">

          <div class="intro__column <?php if (empty($video)): echo '--no-video'; endif; ?>">
            <?php if (isset($subheader) && !empty($subheader)) : ?>
              <div class="intro__top-header">
                <div class="secondary-header"><?= $subheader; ?></div>
              </div>
            <?php endif; ?>
            <?php if (isset($title) && !empty($title)) : ?>
              <div class="intro__header ">
                <h1><?= $title; ?></h1>
              </div>
            <?php endif; ?>
            <?php if (isset($paragraph) && !empty($paragraph)) : ?>
              <div class="intro__body">
                <p><?= $paragraph; ?></p>
              </div>
            <?php endif; ?>
          </div>

          <?php if (isset($video) && !empty($video)) : ?>
            <div class="intro__column">
              <div class="intro__embed-container">
                <iframe src='https://www.youtube.com/embed/<?= $video; ?>' frameborder='0' allowfullscreen class="intro__video"></iframe>
              </div>
            </div>
          <?php endif; ?>

        </div>
      </div>
    </div>

<?php endif; ?>
