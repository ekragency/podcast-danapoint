<section>
  <div class="section">
    <div class="section__container">
      <div class="intro">
        <div class="intro__header">
          <h1> All Archives </h1>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="section">
    <div class="section__container">
      <div class="page-body">
        <div class="page-body__content">
          <ul>
            <?php wp_get_archives( array(
              'format' => 'custom',
              'before' => '<li>',
              'after'  => '</li>',
            )); ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
