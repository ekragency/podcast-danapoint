<?php
  extract($partialData);
  $blocks = get_sub_field('ac_blocks');
?>

<section>
  <div class="section --amenity-bg">
    <div class="section__container">

      <!-- Amenity Content Blocks -->
      <div class="amenity">

        <div class="amenity__blocks">

          <?php if( $blocks ): $counter = 1; foreach( $blocks as $amenityBlock ): ?>
            <div class="image-block --amenity">
              <div class="image-block__container">
                <div class="image-block__content">
                    <div class="image-block__image --square js-background-cover background-cover">
                      <?php if (!empty($amenityBlock['ac_block_image']['url'])): ?>
                          <img src="<?php echo $amenityBlock['ac_block_image']['url'] ?>" alt="<?php echo $amenityBlock['ac_block_image']['alt'] ?>">
                      <?php endif; ?>
                      <?php if ( !empty($amenityBlock['ac_block_url']) ): ?>
                        <a class="image-block__image-link" href="<?php echo $amenityBlock['ac_block_url']['url'] ?>" title="<?php echo $amenityBlock['ac_blocks_title'] ?>"></a>
                      <?php endif; ?>
                    </div>
                  <div class="image-block__header">
                    <h3 class="sub-header">
                      <?php if (!empty($amenityBlock['ac_block_url'])) : ?>
                        <a href="<?php echo $amenityBlock['ac_block_url']['url'] ?>" title="<?php echo $amenityBlock['ac_blocks_title'] ?>"><?php echo $amenityBlock['ac_blocks_title'] ?></a>
                      <?php else : ?>
                        <?php echo $amenityBlock['ac_blocks_title'] ?>
                      <?php endif; ?>
                    </h3>
                  </div>
                  <div class="image-block__separator"></div>
                  <div class="image-block__description">
                    <ul>
                      <li><?php echo $amenityBlock['ac_blocks_amenity_one'] ?></li>
                      <li><?php echo $amenityBlock['ac_blocks_amenity_two'] ?></li>
                      <li><?php echo $amenityBlock['ac_blocks_amenity_three'] ?></li>
                    </ul>
                  </div>
                  <?php if ( !empty($amenityBlock['ac_block_url']) ): ?>
                    <div class="image-block__button">
                      <a href="<?php echo $amenityBlock['ac_block_url']['url'] ?>" class="button__link" title="<?php echo $amenityBlock['ac_blocks_button_text'] ?>"><?php echo $amenityBlock['ac_blocks_button_text'] ?></a>
                    </div>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          <?php $counter++; endforeach; endif; ?>

        </div>

      </div>
    </div>
  </div>
</section>
