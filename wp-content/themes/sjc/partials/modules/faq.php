<?php $faqItems = $partialData; ?>
<?php if(isset($faqItems) && !empty($faqItems)) : ?>
  <section>
    <div class="section">
      <div class="section__container --short">
        <div class="page-body">
          <div class="page-body__content">
            <div class="faq-page">
              <?php foreach ($faqItems as $item) : ?>
                <div class="faq-page__item js-accordion">
                  <div class="faq-page__header"><button class="tertiary-header js-accordion-toggle"><?php echo $item["faq_item"]; ?></button></div>
                  <div class="faq-page__answer js-accordion-target"><?php echo $item["faq_answer"]; ?></div>
                </div>
              <?php endforeach; ?>
            </div>
          </div>
          <div class="page-body__sidebar">
            <?php \NobleStudios\Utilities\Tools::renderPartial("modules/page-sidebar");?>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
