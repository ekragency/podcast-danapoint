<?php
  $background   = get_sub_field('tcb_bg');
  $subHeader    = get_sub_field('tcb_header');
  $tag          = get_sub_field('tcb_tag');
  $paragraph    = get_sub_field('tcb_paragraph');
  $subImage     = get_sub_field('tcb_content_sub_image');
  $link         = get_sub_field('tcb_link');
  $image        = get_sub_field('tcb_featured_image');
  $reverseCol   = get_sub_field('tcb_reverse_columns');
  $reverseFont  = get_sub_field('tcb_reverse_font_color');
?>
<section>
  <div class="section">
    <div class="section__container --full-width --no-top-padding --no-bottom-padding <?php echo ($background ? '--background-image  js-background-cover background-cover' : ''); ?>">
      <?php if( isset($background) && !empty($background) ): ?>
        <img src="<?php echo $background['url'] ?>" alt="<?php echo $background['alt']; ?>">
      <?php endif; ?>
      <div class="content-block <?php echo $reverseCol ? '--reversed' : ''; ?>">
        <div class="content-block__small">
            <div class="content-block__tag <?php echo $reverseFont ? '--is-legible' : ''; ?>">
              <?php if( isset($tag) && $tag ): ?>
                <h5> <?php echo $tag; ?> </h5>
              <?php endif; ?>
            </div>
            <div class="content-block__header <?php echo $reverseFont ? '--is-legible' : ''; ?>">
              <?php if( isset($subHeader) && !empty($subHeader) ): ?>
                <h2> <?php echo $subHeader; ?> </h2>
              <?php endif; ?>
            </div>
            <div class="content-block__body <?php echo $reverseFont ? '--is-legible' : ''; ?>">
              <?php if( isset($paragraph) && !empty($paragraph) ): ?>
               <p> <?php echo $paragraph; ?> </p>
              <?php endif; ?>
            </div>
            <?php if( isset($link['title']) && !empty($link['title']) ): ?>
              <div class="content-block__link">
                <a href="<?php echo $link['url']; ?>" class="link__cta <?php echo $reverseFont ? '--is-legible' : ''; ?>" <?php echo ($link['target'] ? 'target="_blank"' : '' ); ?>>
                  <?php echo $link['title']; ?>
                </a>
              </div>
            <?php endif; ?>
            <?php if( isset($subImage) && !empty($subImage) ) :?>
              <div class="content-block__sub-image">
                <img src="<?php echo $subImage; ?>" alt="">
              </div>
            <?php endif; ?>
          </div>
          <div class="content-block__big">
            <div class="content-block__big-background js-background-cover background-cover">
              <picture>
                <source srcset="<?php echo $image['url'] ?>" media="(min-width: 768px)">
                <img srcset="<?php echo $image['url'] ?>" alt="<?php echo $image['alt'] ?>">
              </picture>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
