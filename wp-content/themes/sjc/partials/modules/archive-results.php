<div class="archive-page__results">
  <ul class="archive-results__items">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <?php $image = \NobleStudios\Helpers\getFeaturedImage(get_the_ID()); ?>
       <li class="archive-results__item">
         <article>
           <div class="archive-results__item-container">
           <?php if($image) : ?>
               <div class="archive-results__image-block">
                 <aside>
                  <a href="<?php the_permalink(); ?>"><img src="<?php echo $image; ?>"class="archive-results__image" alt="<?php the_title(); ?>"></a>
                 </aside>
               </div>
            <?php endif; ?>
               <div class="archive-results__content">
                 <div class="archive-results__header">
                   <span class="sub-alt-header archive-results__meta"><?php echo get_the_date(); ?></span>
                   <a href="<?php the_permalink(); ?>"><h4 class="sub-header-alt"><?php the_title(); ?></h4></a>
                 </div>
                   <?php the_excerpt();?>
               </div>
            </div>
         </article>
       </li>
    <?php endwhile; else: ?>
      <li><p> <?php _e( 'Sorry, no posts matched your criteria.' ); ?> </p></li>
    <?php endif; ?>
  </ul>
</div>
