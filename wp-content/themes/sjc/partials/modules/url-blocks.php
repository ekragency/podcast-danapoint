<?php
  $rowData = $partialData;
  $desktop = get_sub_field('url_background_desktop');
  $tablet = get_sub_field('url_background_tablet');
  $mobile = get_sub_field('url_background_mobile');
  $blockHeader = get_sub_field('url_block_header');
  $blockSubheader = get_sub_field('url_block_subheader');
  $blocks = get_sub_field('url_blocks');
  $link = get_sub_field('url_block_link');

  if( empty($link['title']) ) {
    $link['title'] = 'Learn More';
  }

  $columns = get_sub_field('url_block_columns');

  if (isset($columns) && !empty($columns) && is_numeric($columns)) {
      $columns = "--$columns-columns";
  } else {
      $columns = '';
  }

  // Hide the separator div if we have the last row
  $hideSeparator = ( $rowData['rowCounter'] == $rowData['maxRows'] ? true : false );
?>

<section>
  <div class="section js-background-cover background-cover">
    <div class="section__container">

        <picture>
          <?php if( !empty($desktop) ) : ?>
            <source srcset="<?php echo $desktop; ?>" media="(min-width: 1140px)">
          <?php endif; ?>
          <?php if( !empty($tablet) ) : ?>
            <source srcset="<?php echo $tablet; ?>" media="(min-width: 450px)">
          <?php endif; ?>
          <?php if( !empty($mobile) ) : ?>
            <img srcset="<?php echo $mobile; ?>" alt="Hero Image">
          <?php endif; ?>
        </picture>
<!-- URL Blocks -->
      <div class="featured">
        <?php if( isset($blockHeader) && $blockHeader ): ?>
        <div class="featured__header">
            <h2><?php echo $blockHeader; ?></h2>
            <?php if( isset($blockSubheader) &&  !empty($blockSubheader) ): ?>
              <p><?php echo $blockSubheader; ?></p>
            <?php endif; ?>
        </div>
        <?php endif; ?>

        <?php if( isset( $link['url'] ) && !empty( $link['url']) ): ?>
            <div class="featured__button">
                <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="button__link"> <?php echo $link['title']; ?> </a>
            </div>
        <?php endif; ?>

        <div class="featured__blocks js-slider-mobile">
          <?php if( $blocks ): $counter = 1; foreach( $blocks as $linkedBlock ): ?>
            <div class="image-block gm-block-<?php echo $counter; ?> <?php echo $columns ?>">
              <a href="<?php echo $linkedBlock['url_blocks_url']['url'] ?>" target="<?php echo $linkedBlock['url_blocks_url']['target'] ?>" class="image-block__container">
                <div class="image-block__content">
                  <div class="image-block__image <?php echo $columns ?> js-background-cover background-cover">
                    <?php if (!empty($linkedBlock['url_block_image']['url'])): ?>
                        <img src="<?php echo $linkedBlock['url_block_image']['url'] ?>" alt="<?php echo $linkedBlock['url_block_image']['alt'] ?>">
                    <?php endif; ?>
                  </div>
                  <div class="image-block__header">
                    <h3 class="sub-header"> <?php echo $linkedBlock['url_blocks_title'] ?> </h3>
                  </div>
                </div>
              </a>
            </div>
          <?php $counter++; endforeach; endif; ?>
        </div>

      </div>
    </div>
  </div>
</section>
