<?php
  // Build the footer navigation tree
  $footerNavigation = null;
  if (has_nav_menu('footer-links') && class_exists('NobleStudios\App\MenuBuilder')) {
      $footerNavigation = NobleStudios\App\MenuBuilder::buildNav('Footer Menu');
  }
?>

<div class="site-footer__links">
  <ul class="site-footer-link__items">
    <?php if (!empty($footerNavigation)): ?>
        <?php foreach( $footerNavigation as $footerLink ): ?>
          <li class="site-footer-link__item">
            <a href="<?php echo $footerLink->url; ?>"<?php if($footerLink->target === '_blank') : ?> target="_blank"<?php endif; ?>> <?php echo $footerLink->title; ?> </a>
          </li>
        <?php endforeach; ?>
    <?php endif; ?>
  </ul>
</div>
