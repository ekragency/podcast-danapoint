<?php
global $post;
?>
<!-- Cookie Bar -->
<div class="site-footer__cookies --open" id="site-footer__cookies" style="display: none;">
    <div class="site-footer__cookies-text">
        <div class="site-footer__cookies-text-item --text">
            <p class="cookie-text">We use cookies on this site to enhance your user experience. By continuing to use the site, you agree to our cookie policy. <a href="/privacy-policy/" target="_blank"> More Information</a></p>
        </div>
        <div class="site-footer__cookies-text-item --button">
            <button type="button" class="site-footer__cookies-close button__link --secondary" aria-label="use of cookies close button" id="closeCookie">OK, I AGREE</button>
        </div>
    </div>
</div>
