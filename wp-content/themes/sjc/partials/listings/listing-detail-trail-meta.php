<?php $stakeholderDetails = $partialData; ?>

<!--info -->
<div class="detail-head__info">
  <div class="detail-head__taxonomy">
    <div class="detail-head__taxonomy-items">
      <?php if( $stakeholderDetails['trail_distance'] ): ?>
        <div class="detail-head__taxonomy-item"><span class="detail-head__taxonomy-label">Distance: </span> <?php echo $stakeholderDetails['trail_distance']; ?> </div>
      <?php endif; ?>
      <?php if( $stakeholderDetails['trail_elevation'] ): ?>
        <div class="detail-head__taxonomy-item"><span class="detail-head__taxonomy-label">Elevation: </span> <?php echo $stakeholderDetails['trail_elevation']; ?> </div>
      <?php endif; ?>
      <?php if( $stakeholderDetails['trail_difficulty'] ): ?>
        <div class="detail-head__taxonomy-item"><span class="detail-head__taxonomy-label">Difficulty: </span> <?php echo $stakeholderDetails['trail_difficulty']; ?> </div>
      <?php endif; ?>
      <?php if( $stakeholderDetails['trail_time'] ): ?>
        <div class="detail-head__taxonomy-item"><span class="detail-head__taxonomy-label">Time: </span> <?php echo $stakeholderDetails['trail_time']; ?> </div>
      <?php endif; ?>
      <?php if( $stakeholderDetails['trail_start'] ): ?>
        <div class="detail-head__taxonomy-item"><span class="detail-head__taxonomy-label">Begin at: </span> <?php echo $stakeholderDetails['trail_start']; ?> </div>
      <?php endif; ?>
      <?php if( $stakeholderDetails['trail_features'] ): ?>
        <div class="detail-head__feature-list"> <span> <?php echo $stakeholderDetails['trail_features']; ?> </span> </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="detail-head__contact">
    <div class="detail-head__contact-item">
      <a href="http://maps.google.com?q=<?php echo $stakeholderDetails['map_location']['address']; ?>" target="_blank" class="icon-marker link__cta">
        Get Directions
      </a>
    </div>
    <?php if( $stakeholderDetails['trail_map_link'] ): ?>
      <div class="detail-head__contact-item">
        <a href="<?php echo $stakeholderDetails['trail_map_link']; ?>" target="_blank" class="icon-map link__cta">
          Trail Map
        </a>
      </div>
    <?php endif;  ?>
  </div>
</div>
