<?php
$seoContentBlock = $partialData['seoContentBlock'];
?>

<div class="listing-grid js-listing-view">
  <?php NobleStudios\Utilities\Tools::renderPartial('js-templates/grid-block-featured'); ?>
  <?php NobleStudios\Utilities\Tools::renderPartial('js-templates/grid-blocks'); ?>
  <?php NobleStudios\Utilities\Tools::renderPartial('js-templates/offers-grid-block'); ?>
  <?php NobleStudios\Utilities\Tools::renderPartial('js-templates/no-results-found'); ?>
</div>

<div class="listing-grid__button js-listing-pager --hidden">
  <button class="button__button">Load More</button>
</div>

<?php if($partialData['seoContentBlock']) : ?>
  <div class="seo-content-block"><?= $seoContentBlock ?></div>
<?php endif; ?>
