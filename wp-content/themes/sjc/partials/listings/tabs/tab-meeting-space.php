<?php $meetingSpace = $partialData; ?>

<div class="detail-tab js-tab" data-tab-title="meetings">
  <div class="detail-tab__list-container">
    <ul class="detail-tab__list-items">

      <?php if( $meetingSpace['meetingSqft'] ): ?>
        <li class="detail-tab__list-item">
          Square Ft: <?php echo $meetingSpace['meetingSqft']; ?>
        </li>
      <?php endif; ?>

      <?php if( $meetingSpace['numRooms'] ): ?>
        <li class="detail-tab__list-item">
          Meeting Rooms: <?php echo number_format($meetingSpace['numRooms']); ?>
        </li>
      <?php endif; ?>

      <?php if( $meetingSpace['numSuites'] ): ?>
        <li class="detail-tab__list-item">
          Sleeping Rooms: <?php echo number_format($meetingSpace['numSuites']); ?>
        </li>
      <?php endif; ?>

      <?php if( $meetingSpace['capacity'] ): ?>
        <li class="detail-tab__list-item">
          Capacity: <?php echo $meetingSpace['capacity']; ?>
        </li>
      <?php endif; ?>
    </ul>
  </div>
</div>
