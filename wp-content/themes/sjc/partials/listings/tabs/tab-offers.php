<?php $offers = $partialData; ?>

<div class="detail-tab js-tab" data-tab-title="offers">
  <div class="detail-tab__list-container">
    <ul class="offers-list__items">
      <?php foreach( $offers as $offer ): ?>
        <li class="offers-list__item" id="<?php echo $offer['stakeholder_offer_slug']; ?>">
          <article>
            <div class="offers-list__item-container">
              <?php if( $offer['stakeholder_offer_image'] ): ?>
                <div class="offers-list__image-block">
                  <aside>
                    <img src="<?php echo $offer['stakeholder_offer_image'] ?>" class="offers-list__image" alt="<?php echo $offer['stakeholder_offer_name']; ?>">
                  </aside>
                </div>
              <?php endif; ?>
                <div class="offers-list__content">
                  <div class="offers-list__header">
                    <h1 class="sub-header"> <?php echo $offer['stakeholder_offer_name']; ?> </h1>
                  </div>
                  <p> <?php echo $offer['stakeholder_offer_description']; ?>

                  <?php if (empty($offer['stakeholder_offer_link']) === false) : ?>
                    <br />
                    <div class="detail-tabs__button" ><a href="<?= $offer['stakeholder_offer_link'] ?>" class="button__link"  target="_blank">View Offer</a></div>
                  <?php endif ?>
                  </p>
                </div>
            </div>
          </article>
         </li>
      <?php endforeach; ?>
    </ul>
  </div>
</div>
