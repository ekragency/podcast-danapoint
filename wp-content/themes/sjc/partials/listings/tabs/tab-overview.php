<?php $stakeholderDetails = $partialData;

  $petFriendly = false;


  //Is it directly defined as pet friendly?
  if ($stakeholderDetails['pet_friendly'])
  {
    $petFriendly = true;
  }

  //Check for the amenity if it isn't.
  if ($petFriendly === false && empty($stakeholderDetails['amenities']) === false)
  {
    foreach ($stakeholderDetails['amenities'] as $amenity)
    {
      if ($amenity->slug === 'pet-friendly')
      {
        $petFriendly = true;
        break;
      }
    }
  }

 ?>

<div class="detail-tab --active js-tab" data-tab-title="overview">
  <div class="detail-tab__large-content">
    <div class="detail-tab__expanding">
      <?php if( $stakeholderDetails['ovw_header'] ): ?>
        <div class="detail-tab__header">
          <h3> <?php echo $stakeholderDetails['ovw_header']; ?> </h3>
        </div>
      <?php endif; ?>
      <?php if( $stakeholderDetails['ovw_description'] ): ?>
        <p> <?php echo $stakeholderDetails['ovw_description']; ?> </p>
      <?php endif; ?>
    </div>

    <?php if($petFriendly === true): ?>
      <div class="detail-tab__pet-friendly icon-paw-print">
      Pet Friendly
      </div>
    <?php endif; ?>

    <div class="detail-tab__interactions">
      <?php if( $stakeholderDetails['offers'] ): ?>
        <div class="detail-tab__offers">
          <a href="#" class="button__link js-offers-cta" data-icon="tag">
            <?php echo count($stakeholderDetails['offers']); ?>
            <?php echo count($stakeholderDetails['offers']) > 1 ? 'offers' : 'offer'; ?>
            available
          </a>
        </div>
      <?php endif; ?>
    </div>

  </div>

  <div class="detail-tab__small-content">
    <?php if(!empty($stakeholderDetails['map_location']['lat']) && !empty($stakeholderDetails['map_location']['lng'])) : ?>
        <div class="detail-tab__map" id="listing-map"
             data-lat="<?php echo $stakeholderDetails['map_location']['lat']; ?>"
             data-lng="<?php echo $stakeholderDetails['map_location']['lng']; ?>"
             >
        </div>
    <?php endif; ?>
  </div>

</div>
