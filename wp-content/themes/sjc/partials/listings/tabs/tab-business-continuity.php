<?php $businessContinuity = $partialData; ?>
<div class="detail-tab js-tab" data-tab-title="business-continuity">
  <div class="detail-tab__list-container">
    <ul class="detail-tab__list-items">

      <?php if( $businessContinuity ): foreach( $businessContinuity as $businessContinuityItem ): ?>
        <?php if( !empty( $businessContinuityItem ) ): ?>
            <?php 
              if ($businessContinuityItem == 1 ) { continue;} // skip the field that checks if we want to show business continuity fields, label: isBusinessContinuity 
              if (substr( $businessContinuityItem, 0, 4 ) === "http" ) {

                echo '<li class="detail-tab__list-item"><a target="_blank" href="'. $businessContinuityItem . '">Click to View Health and Safety Policy</a></li>';
                continue;
              }
            
            ?> 
            <li class="detail-tab__list-item"><?php echo $businessContinuityItem; ?></li>
        <?php endif; ?>
      <?php endforeach;  endif; ?>
    </ul>
  </div>
</div>
