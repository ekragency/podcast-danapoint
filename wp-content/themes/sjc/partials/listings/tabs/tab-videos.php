<?php $videos = $partialData; 

?>

<div class="detail-tab js-tab" data-tab-title="videos">
  <div class="detail-tab__list-container">
      <?php foreach( $videos as $video ): ?>

        <?php if (empty($video['stakeholder_video']) === false) : ?>
          <div class="detail-tab__video">
            <iframe width="560" height="315" src="<?php echo $video['stakeholder_video']; ?>" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
        <?php endif; ?>


      <?php endforeach; ?>
  </div>
</div>
