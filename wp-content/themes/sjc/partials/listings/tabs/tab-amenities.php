<?php $amenities = $partialData ?>
<div class="detail-tab js-tab" data-tab-title="amenities">
  <div class="detail-tab__list-container">
    <ul class="detail-tab__list-items">
      <?php if( $amenities ): foreach( $amenities as $amenity ): ?>
        <li class="detail-tab__list-item"> <?php echo $amenity->name; ?> </li>
      <?php endforeach;  endif; ?>
    </ul>
  </div>
</div>
