<?php
    $stakeholderDetails = $partialData;
    $backgroundMobile = '/wp-content/themes/visitdanapoint/assets/images/stakeholder-bg_m.jpg'; 
    $background = '/wp-content/themes/visitdanapoint/assets/images/stakeholder-bg.jpg';
?>
<section>
  <div class="section">
    <div class="section__container --no-hero --full-width-padding js-background-cover background-cover detail-head__section detail-head__section-background">
        <picture>
          <source srcset="<?= $background; ?>" media="(min-width: 1140px)">
            <source srcset="<?= $background; ?>" media="(min-width: 450px)">
            <img srcset="<?= $backgroundMobile; ?>" alt="Hero Image">
        </picture>
        <div class="detail-head__content">
          <?php \NobleStudios\Utilities\Tools::renderPartial("listings/listing-detail-header", $stakeholderDetails); ?>
        </div>
    </div>
  </div>
</section>

<section>
    <div class="section">
      <div class="section__container --full-width detail-tabs__section-background">
        <?php \NobleStudios\Utilities\Tools::renderPartial("listings/listing-detail-tabs", $stakeholderDetails); ?>
      </div>
    </div>
</section>
