<?php
    $stakeholderDetails = $partialData;
    $address = "$stakeholderDetails[address1] $stakeholderDetails[address2] $stakeholderDetails[address3], $stakeholderDetails[city] $stakeholderDetails[state] $stakeholderDetails[zip]";
?>

<!--info -->
<div class="detail-head__info">
  <!-- stakeholder logo -->
  <?php if( !empty($stakeholderDetails['logo']) ) : ?>
    <div class="detail-head__logo">
      <img src="<?= $stakeholderDetails['logo']; ?>" alt="<?= get_the_title(); ?> Logo" class="detail-head__logo-image">
    </div>
  <?php endif; ?>
  <!-- taxonomy -->
  <div class="detail-head__taxonomy">
    <div class="detail-head__taxonomy-items">
      <?php if( $stakeholderDetails['primary_category'] ): ?>
      <?php // TODO let's figure out how to get the Primary Category, ask Nitish ?>
        <div class="detail-head__taxonomy-item"><span class="detail-head__taxonomy-label">Category: </span> <?php echo $stakeholderDetails['primary_category']; ?> </div>
      <?php endif; ?>
      <?php if( $stakeholderDetails['neighborhood'] ): ?>
        <div class="detail-head__taxonomy-item"><span class="detail-head__taxonomy-label">Neighborhood: </span> <?php echo $stakeholderDetails['neighborhood']; ?> </div>
      <?php endif; ?>
    </div>
  </div>
  <div class="detail-head__contact">
    <?php if( $address ): ?>
      <div class="detail-head__contact-item">
        <a href="http://maps.google.com?q=<?= $address; ?>" target="_blank" id="gtm-location-click" class="icon-marker-stroke link__cta">
            <?= $stakeholderDetails['address1'] ?><br>
            <?php if (!empty($stakeholderDetails['address2'])): ?>
                <?= $stakeholderDetails['address2'] ?><br>
            <?php endif; ?>
            <?php if (!empty($stakeholderDetails['address3'])): ?>
                <?= $stakeholderDetails['address3'] ?><br>
            <?php endif; ?>
            <?= "$stakeholderDetails[city] $stakeholderDetails[state] $stakeholderDetails[zip]" ?>
        </a>
      </div>
    <?php endif; ?>
    <?php if( $stakeholderDetails['phone'] ): ?>
      <div class="detail-head__contact-item">
        <a href="tel:<?php echo $stakeholderDetails['phone'] ?>" id="gtm-phone-click" class="icon-phone link__cta">
          <?php echo $stakeholderDetails['phone']; ?>
        </a>
      </div>
    <?php endif; ?>
    <?php if( $stakeholderDetails['website_link'] ): ?>
      <div class="detail-head__contact-item">
        <a href="<?php echo $stakeholderDetails['website_link']; ?>" target="_blank" id="gtm-website-click" class="icon-globe link__cta">
          Visit Website
        </a>
      </div>
    <?php endif; ?>
  </div>
  <?php if( $stakeholderDetails['reservation_link'] ): ?>
    <div class="detail-head__button">
      <a href="<?php echo $stakeholderDetails['reservation_link'] ?>" target="_blank" id="gtm-reservation-click" class="button__link">
        Book Now
      </a>
    </div>
  <?php endif; ?>
</div>
