<?php $listingOptions = $partialData;
	$offerDisplay = false;
	if (isset($listingOptions['offerDisplay']) && $listingOptions['offerDisplay'] === true) {
		$offerDisplay = true;
    $listingOptions['type'] = "Places to Stay";
  }
?>


<section class="js-listing-post-type" data-post-type="<?php echo $listingOptions['postType']; ?>" <?php echo $offerDisplay === true ? 'data-offer-display="true"' : ''; ?>>
  <div class="section">
    <div class="section__container">
      <div class="spinner-container js-spinner"><div class="spinner"></div></div>
      <?php \NobleStudios\Utilities\Tools::renderPartial("listings/filters/filter-bar", $listingOptions); ?>
      <?php \NobleStudios\Utilities\Tools::renderPartial("listings/grid", $listingOptions); ?>
      <?php \NobleStudios\Utilities\Tools::renderPartial('listings/filters/offers-pagination', array()); ?>
    </div>
  </div>
</section>
