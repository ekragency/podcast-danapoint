<?php
$options = $partialData;
// this is a hacky workaround to avoid an issue with the filtering system.
// if you refer to _addExcludedPostsParam() in the theme folder, App/NobleStudios/stakeholderHelpers.php
// you'll see that we implemented a custom filtering tool that's not displaying the correct offers under the
// Things to Do filter.
// array_pop($options['values']);

?>




<button class="button__link --active --offers-filter  js-meta-button-filter" data-value="Places to Stay" data-key="stakeholder_type"> Places to Stay </button>
<button class="button__link --offers-filter  js-meta-button-filter" data-value="Dining" data-key="stakeholder_type"> Dining </button>
<button class="button__link --offers-filter  js-meta-button-filter" data-value="Things to Do" data-key="stakeholder_type"> Things to Do </button>
<button class="button__link --offers-filter js-meta-button-filter" data-value="-1"  data-key="<?php echo (isset($options['key']) ? $options['key'] : '') ?>"> All </button>
    <!-- <button class="button__link --offers-filter js-meta-button-filter" data-value="-1" data-key="stakeholder_type"> Things to Do </button> -->



