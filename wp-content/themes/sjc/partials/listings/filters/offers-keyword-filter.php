<?php $options = $partialData; ?>

<div class="filter__option  --offers-search <?php echo (isset($options['classes']) ? $options['classes'] : '') ?>">
  <input type="text" id="search" class="--offers-search js-keyword-filter" placeholder="Search Offers" data-key="<?php echo (isset($options['key']) ? $options['key'] : '') ?>" />
</div>
