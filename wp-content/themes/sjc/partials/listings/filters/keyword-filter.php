<?php $options = $partialData; ?>

<div class="filter__option <?php echo (isset($options['classes']) ? $options['classes'] : '') ?>">
  <input type="text" id="search" class="js-keyword-filter" placeholder="Keyword" data-key="<?php echo (isset($options['key']) ? $options['key'] : '') ?>" />
</div>
