<?php
  // Grab the taxonomy/label/All the things
  $options = $partialData;
  $terms = array();
  if( isset($options['taxonomy']) && $options['taxonomy'] )
    $terms = get_terms($options['taxonomy'], 'hide_empty=0');
?>
<div class="filter__option <?php echo (isset($options['classes']) ? $options['classes'] : '') ?>">
  <select name="" class="js-taxonomy-filter" data-key="<?php echo (isset($options['taxonomy']) ? $options['taxonomy'] : '' ) ?>">
    <?php if( isset($options['label']) && $options['label'] ): ?>
      <option value='-1'>
         <?php echo $options['label']; // First option can be the "All" option ?>
      </option>
    <?php endif; ?>
    <?php foreach($terms as $term): ?>
      <option value='<?php echo $term->slug; ?>'>
        <?php echo $term->name; ?>
      </option>
    <?php endforeach; ?>
  </select>
</div>
