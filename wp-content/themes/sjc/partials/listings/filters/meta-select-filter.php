<?php $options = $partialData; ?>
<div class="filter__option <?php echo (isset($options['classes']) ? $options['classes'] : '') ?>">
  <select name="" class="js-meta-filter" data-key="<?php echo (isset($options['key']) ? $options['key'] : '') ?>">
    <?php if( isset($options['label']) && $options['label'] ): ?>
      <option value='-1'>
         <?php echo $options['label']; // First option can be the "All" option ?>
      </option>
    <?php endif; ?>
    <?php foreach( $options['values'] as $key => $value ): ?>
      <option value="<?php echo $value ?>"> <?php echo $key ?> </option>
    <?php endforeach; ?>
  </select>
</div>
