<?php $options = $partialData; ?>
<div class="filter__option <?php echo ($options['classes'] ? $options['classes'] : '') ?>">
  <input type="checkbox" id="checkbox<?php echo ($options['key'] ? '-'.$options['key'] : ''); ?>" class="js-meta-filter" data-key="<?php echo ($options['key'] ? $options['key'] : ''); ?>">
  <label for="checkbox<?php echo ($options['key'] ? '-'.$options['key'] : ''); ?>">
    <?php echo ($options['label'] ? $options['label'] : ''); ?>
  </label>
</div>
