<?php
  // This special marshmallow of a filter renders one filter which is "sticky" but at the
  // same time be able to filter children terms
  $options = $partialData;
  $terms = array();
  // We might have a prebuilt taxonomy so ensure data exists and fire away
  if( isset($options['children']) ) $terms = $options['children'];
?>
<div class="filter__option <?php echo (isset($options['classes']) ? $options['classes'] : '') ?>">
  <select name="" class="js-taxonomy-filter" data-key="<?php echo (isset($options['taxonomy']) ? $options['taxonomy'] : '' ) ?>">
    <?php if( isset($options['label']) && $options['label'] ): ?>
      <option value='<?php echo $options['term'] ?>'>
         <?php echo $options['label']; // First option will be the parent category option ?>
      </option>
    <?php endif; ?>
    <?php foreach($terms as $term): ?>
      <option value='<?php echo $term->slug; ?>'>
        <?php echo $term->name; ?>
      </option>
    <?php endforeach; ?>
  </select>
</div>
