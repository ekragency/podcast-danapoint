<?php
  // This is a sticky filter. This means that the taxonomy filter is hardcoded on page load
  // The user should not be able to change the filter
  $options = $partialData;
?>
<div class="filter__option <?php echo (isset($options['classes']) ? $options['classes'] : '') ?>">
  <select name="" class="js-sticky-filter js-select-disabled"
          data-key="<?php echo (isset($options['taxonomy']) ? $options['taxonomy'] : '' ) ?>"
          data-sticky="<?php echo (isset($options['term']) ? $options['term'] : '' ) ?>">
    <?php if( isset($options['label']) && $options['label'] ): ?>
      <option value='<?php $options['term']; ?>'>
         <?php echo $options['label']; ?>
      </option>
    <?php endif; ?>
  </select>
</div>
