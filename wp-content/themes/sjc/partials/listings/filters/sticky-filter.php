<?php // This is just a regular old, run of the mill, sticky filter. Nothing special ?>
<?php $options = $partialData; ?>

<div class="filter__option <?php echo (isset($options['classes']) ? $options['classes'] : '') ?>">
  <div class="js-sticky-filter"
    data-key="<?php echo (isset($options['key']) ? $options['key'] : '') ?>"
    data-sticky="<?php echo (isset($options['value']) ? $options['value'] : '') ?>"></div>
</div>
