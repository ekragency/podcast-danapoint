<?php $options = $partialData; ?>

<div class="filter__option <?php echo (isset($options['classes']) ? $options['classes'] : '') ?>">
  <input type="text" class="js-sticky-filter js-meta-filter"
    data-key="<?php echo (isset($options['key']) ? $options['key'] : '') ?>"
    value="<?php echo (isset($options['value']) ? $options['value'] : '') ?>"/>
</div>
