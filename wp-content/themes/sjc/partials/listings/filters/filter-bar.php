<?php
  // Grab filter options like post type and stakeholder type
  $filterOptions = $partialData;
?>
<div class="filter js-listing-filters" data-stakeholder-type="<?php echo ( isset($filterOptions['type']) ? $filterOptions['type'] : '' ) ?>">
  <div class="filter__header">
    <?php if (empty($filterOptions['hideFilterHeader']) === true || $filterOptions['hideFilterHeader'] !== true) : ?>
    
    <button class="sub-header filter__header-button js-filter-collapse">Filter</button>

  <?php endif; ?>
    <div class="filter__header-image"></div>
  </div>
  <div class="filter__options js-filter-collapse-container  <?php echo (isset($filterOptions['defaultFiltersOpen']) && $filterOptions['defaultFiltersOpen'] == true ? '--open --hideBlackBar' : ''); ?>">
    <?php
      // If we're working with a stakeholder of type "Thing to Do", we need to conditionally render
      // the category. The category ends up actually being a subset of the parent category
      if( isset($filterOptions['type']) && $filterOptions['type'] == 'Things to Do' ) {
        // Don't bother rendering the filter if there are no children
        if( isset($filterOptions['children']) && count($filterOptions['children']) > 0 ) {
          \NobleStudios\Utilities\Tools::renderPartial('listings/filters/taxonomy-children-filter', array(
            'taxonomy'  => $filterOptions['taxonomy'],
            'label'     => $filterOptions['label'],
            'term'      => $filterOptions['currentTerm']->slug,
            'children'  => $filterOptions['children'],
          ));
        } else {
          // Otherwise, we have a filter that basically acts like a sticky filter
          if( isset($filterOptions['taxonomy']) ) { // Seat belt check
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/taxonomy-sticky-filter',
            array(
              'taxonomy'  => $filterOptions['taxonomy'],
              'term'      => $filterOptions['currentTerm']->slug,
              'label'     => $filterOptions['currentTerm']->name,
            ));
          }
        }
    // Places to stay and Dining sub listings
    } elseif (isset($filterOptions['offerDisplay']) && $filterOptions['offerDisplay'] == true ){
      if( in_array('offer_type', $filterOptions['filters']) ) {
        \NobleStudios\Utilities\Tools::renderPartial('listings/filters/meta-select-buttons', array(
          'key'       => 'stakeholder_type',
          'values'    => array(
            'Dining' => 'Dining',
            'Place to Stay' => 'Places to Stay',
            'Thing to Do' => 'Things to Do'
          ),
          'label'     => 'All',
          'classes'   => ' --medium ',
        ));
      } 


    } elseif (
        isset($filterOptions['type']) && isset($filterOptions['currentTerm']) && !empty($filterOptions['currentTerm'])
    ) {
         //If we have currentTerm go ahead and render a sticky filter
          \NobleStudios\Utilities\Tools::renderPartial('listings/filters/taxonomy-sticky-filter',
          array(
            'taxonomy'  => $filterOptions['taxonomy'],
            'term'      => $filterOptions['currentTerm']->slug,
            'label'     => $filterOptions['currentTerm']->name,
          ));
      } else {
        // Otherwise, it's just a normal everything makes sense taxonomy filter
        if( isset($filterOptions['taxonomy']) ) { // Safety check
          \NobleStudios\Utilities\Tools::renderPartial('listings/filters/taxonomy-filter',
          array(
            'taxonomy'  => $filterOptions['taxonomy'],
            'label'     => $filterOptions['label'],
          ));
        }
      }
    ?>

    <?php if( in_array('neighborhood', $filterOptions['filters']) ) {
      \NobleStudios\Utilities\Tools::renderPartial('listings/filters/taxonomy-filter', array(
        'taxonomy'  => 'neighborhood',
        'label'     => 'All Neighborhoods',
      ));
    } ?>

    <?php if( in_array('budget', $filterOptions['filters']) ) {
      \NobleStudios\Utilities\Tools::renderPartial('listings/filters/meta-select-filter', array(
        'key'       => 'stakeholder_budget',
        'values'    => array(
          '$' => '$',
          '$$' => '$$',
          '$$$' => '$$$',
          '$$$$' => '$$$$'
        ),
        'label'     => 'All Budgets',
        'classes'   => ' --extra-small ',
      ));
    } ?>

    <?php if( in_array('rooms', $filterOptions['filters']) ) {
      \NobleStudios\Utilities\Tools::renderPartial('listings/filters/meta-select-filter', array(
        'key'       => 'bnb_rooms',
        'values'    => array(
          '1' => '1',
          '2' => '2',
          '3' => '3',
          '4' => '4',
          '5+' => '5',
        ),
        'label'     => 'Rooms',
        'classes'   => ' --extra-small ',
      ));
    } ?>

    <?php if( in_array('breakfast', $filterOptions['filters']) ) {
      \NobleStudios\Utilities\Tools::renderPartial('listings/filters/meta-select-filter', array(
        'key'       => 'bnb_type_of_breakfast',
        'values'    => array(
          'Full' => 'full',
          'Continental' => 'continental',
        ),
        'label'     => 'Type of Breakfast',
        'classes'   => ' --small ',
      ));
    } ?>

    <?php if( in_array('space', $filterOptions['filters']) ) {
      \NobleStudios\Utilities\Tools::renderPartial('listings/filters/meta-select-filter', array(
        'key'       => 'stakeholder_meeting_sqft',
        'values'    => array(
          '0 - 1,000'       => '0 - 1,000',
          '1,001 - 5,000'   => '1,001 - 5,000',
          '5,001 - 10,000'  => '5,001 - 10,000',
          '10,001 - 20,000' => '10,001 - 20,000',
          '20,001 +'        => '20,001 +',
        ),
        'label'     => 'Square Footage',
      ));
    } ?>

    <?php if( in_array('distance_from_beach', $filterOptions['filters']) ) {
      \NobleStudios\Utilities\Tools::renderPartial('listings/filters/meta-select-filter', array(
        'key'    => 'stakeholder_distance_from_beach',
        'values' => array(
          '< 1 mile'   => '<1',
          '1-2 miles'  => '1-2',
          '3-4 miles'  => '3-4',
          '>5 miles'   => '>5',
        ),
        'label'     => 'Distance from the Beach',
      ));
    } ?>

    <?php if( in_array('search', $filterOptions['filters']) ) {
      \NobleStudios\Utilities\Tools::renderPartial('listings/filters/keyword-filter', array(
        'labels'   => ' Keyword ',
        'key'      => 's'
      ));
    } ?>



    <?php if( in_array('pets', $filterOptions['filters']) ) {
      \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
        'key'     => 'stakeholder_pet_friendly',
        'label'   => 'Dog Friendly',
        'classes' => ' --small ',
      ));
    } ?>

    <?php if( in_array('meeting', $filterOptions['filters']) ) {
      \NobleStudios\Utilities\Tools::renderPartial('listings/filters/meta-sticky-filter', array(
        'key'     => 'stakeholder_is_meeting_space',
        'value'   => 1,
        'label'   => 'Meeting Space',
        'classes' => ' --hidden ',
      ));
    } ?>

    <?php if (isset($filterOptions['advancedFilters']) && !empty($filterOptions['advancedFilters'])) { ?>
        <a class= "filter__option js-filter-advancedFilters --small" href="#">More Options</a>
    <?php } ?>

    <?php if( get_field('randomize_stakeholders', 'option') ): ?>
      <?php if( in_array('orderby', $filterOptions['filters']) ) {
        \NobleStudios\Utilities\Tools::renderPartial('listings/filters/sticky-filter', array(
          'key'     => 'orderby',
          'value'   => 'rand',
          'label'   => 'Order',
          'classes' => ' --hidden ',
        ));
      } ?>
    <?php endif; ?>
  </div>
  <?php if (isset($filterOptions['advancedFilters']) && !empty($filterOptions['advancedFilters'])) { ?>
      <div class="filter__advancedOptions">
          <?php if( in_array('butler', $filterOptions['advancedFilters']) ) {
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
              'key'     => 'stakeholder_butlerservice',
              'label'   => 'Butler Service',
              'classes' => ' --extra-small ',
            ));
          } ?>

          <?php if( in_array('beach', $filterOptions['advancedFilters']) ) {
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
              'key'     => 'stakeholder_beach_activity_rentals',
              'label'   => 'Beach Activity Rentals',
              'classes' => ' --small ',
            ));
          } ?>

          <?php if( in_array('golf', $filterOptions['advancedFilters']) ) {
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
              'key'     => 'stakeholder_golf',
              'label'   => 'Golf',
              'classes' => ' --extra-small ',
            ));
          } ?>

          <?php if( in_array('restaurants', $filterOptions['advancedFilters']) ) {
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
              'key'     => 'stakeholder_restaurants',
              'label'   => 'Onsite Restaurants',
              'classes' => ' --extra-small ',
            ));
          } ?>

          <?php if( in_array('pets', $filterOptions['advancedFilters']) ) {
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
              'key'     => 'stakeholder_pet_friendly',
              'label'   => 'Pet Friendly',
              'classes' => ' --extra-small ',
            ));
          } ?>

          <?php if( in_array('pool', $filterOptions['advancedFilters']) ) {
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
              'key'     => 'stakeholder_poolspa',
              'label'   => 'Pool',
              'classes' => ' --extra-small ',
            ));
          } ?>

          <?php if( in_array('dayspa', $filterOptions['advancedFilters']) ) {
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
              'key'     => 'stakeholder_spawellness',
              'label'   => 'Spa & Wellness Center',
              'classes' => ' --small ',
            ));
          } ?>

          <?php if( in_array('wateractivities', $filterOptions['advancedFilters']) ) {
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
              'key'     => 'stakeholder_wateractivities',
              'label'   => 'Water Actvities Included',
              'classes' => ' --small ',
            ));
          } ?>

          <?php if( in_array('weddings', $filterOptions['advancedFilters']) ) {
            \NobleStudios\Utilities\Tools::renderPartial('listings/filters/checkbox-filter', array(
              'key'     => 'stakeholder_weddingvenue',
              'label'   => 'Wedding Venue',
              'classes' => ' --extra-small ',
            ));
          } ?>

      </div>

  <?php } ?>
         <?php if (in_array('display',$filterOptions) &&in_array('offersCountBar', $filterOptions['display'])) {
              \NobleStudios\Utilities\Tools::renderPartial('listings/filters/offers-count-bar', array());


          } ?>


</div>
