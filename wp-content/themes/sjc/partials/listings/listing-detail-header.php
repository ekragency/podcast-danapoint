<?php 

use function NobleStudios\StakeholderHelper\getStakeholderImage;

$stakeholderDetails = $partialData; 


$hasNonBlankSlide = false;
if ($stakeholderDetails['slides']) {
  foreach( $stakeholderDetails['slides'] as $slide ) {
    if ($slide['stakeholder_slide'] !== '')
    {
      $hasNonBlankSlide = true;
    }

  }
}


?>




<div class="detail-head">
  <div class="detail-head__header">
    <h1><?php echo get_the_title(); ?></h1>
  </div>

  <div class="detail-head__details">

      <div class="detail-head__slides js-slider --stakeholder">

          <!--slides -->
          <?php 
          if ($hasNonBlankSlide === true)
          {
            foreach( $stakeholderDetails['slides'] as $slide ): ?>
              <div class="detail-head__slide-container">

                <div class="detail-head__slide js-background-cover background-cover">
                    <img src="https://res.cloudinary.com/simpleview/image/upload/<?php echo $slide['stakeholder_slide']; ?>" alt="<?= get_the_title() ?> Image">
                </div>

              </div>

            <?php endforeach; 

          } else {
    
              ?>
              <div class="detail-head__slide-container">

                <div class="detail-head__slide js-background-cover background-cover">
                    <img src="<?php echo getStakeholderImage(get_the_ID(), $stakeholderDetails['stakeholder_type'], true); ?>" alt="<?= get_the_title() ?> Image">
                </div>

              </div>
              <?php

          }

        ?>
      </div>

    <?php
      if( $stakeholderDetails['trail_toggle'] )
        \NobleStudios\Utilities\Tools::renderPartial('listings/listing-detail-trail-meta', $stakeholderDetails);
      else
        \NobleStudios\Utilities\Tools::renderPartial('listings/listing-detail-meta', $stakeholderDetails);
    ?>
  </div>
</div>
