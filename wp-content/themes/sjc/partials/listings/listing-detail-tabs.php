<?php
  // Fetch all the stakeholder things
  $stakeholderDetails = $partialData;
  $offers = $stakeholderDetails['offers'];
  $videos = $stakeholderDetails['videos'];
  $amenities = $stakeholderDetails['amenities'];
  $meetingSpace = $stakeholderDetails['meeting_space'];
  $businessContinuity = $stakeholderDetails['business_continuity'];
  // Grab the "Back" button from the options table
  $listingTemplateMapping = \NobleStudios\Utilities\Configure::read('listingTemplateMapping');
  $backLink = get_field( $listingTemplateMapping[$stakeholderDetails['stakeholder_type']] , 'option' );
?>

<div class="detail-tabs js-steak-tabs" id="detailTabContainer">
  <div class="detail-tabs__bar">
    <ul class="detail-tabs__bar-items">
      <li class="detail-tabs__bar-item --active js-tab-toggle" data-tab-title="overview"><button class="detail-tabs__bar-item-button">overview</button></li>

      <?php if( $amenities ): ?>
        <li class="detail-tabs__bar-item js-tab-toggle" data-tab-title="amenities"><button class="detail-tabs__bar-item-button">amenities</button></li>
      <?php endif; ?>

      <?php if( $meetingSpace && $meetingSpace['isMeetingSpace'] ): ?>
        <li class="detail-tabs__bar-item js-tab-toggle" data-tab-title="meetings"><button class="detail-tabs__bar-item-button">meeting space</button></li>
      <?php endif; ?>

      <?php if( $offers ): ?>
        <li class="detail-tabs__bar-item js-tab-toggle" data-tab-title="offers"><button class="detail-tabs__bar-item-button">offers</button></li>
      <?php endif; ?>

      <?php if( $videos ): ?>
        <li class="detail-tabs__bar-item js-tab-toggle" data-tab-title="videos"><button class="detail-tabs__bar-item-button">videos</button></li>
      <?php endif; ?>

      <?php if( $businessContinuity && $businessContinuity['isBusinessContinuity'] ): ?>
        <li class="detail-tabs__bar-item js-tab-toggle" data-tab-title="business-continuity"><button class="detail-tabs__bar-item-button">health &amp; safety</button></li>
      <?php endif; ?>

    </ul>
  </div>
  <div class="detail-tabs__container --full-width-padding">
    <?php \NobleStudios\Utilities\Tools::renderPartial("listings/tabs/tab-overview", $stakeholderDetails); ?>

    <?php if( $amenities ): ?>
      <?php \NobleStudios\Utilities\Tools::renderPartial("listings/tabs/tab-amenities", $amenities); ?>
    <?php endif; ?>

    <?php if( $meetingSpace ): ?>
      <?php \NobleStudios\Utilities\Tools::renderPartial("listings/tabs/tab-meeting-space", $meetingSpace); ?>
    <?php endif; ?>

    <?php if( $offers ): ?>
      <?php \NobleStudios\Utilities\Tools::renderPartial("listings/tabs/tab-offers", $offers); ?>
    <?php endif; ?>

    <?php if( $videos ): ?>
      <?php \NobleStudios\Utilities\Tools::renderPartial("listings/tabs/tab-videos", $videos); ?>
    <?php endif; ?>


    <?php if( $businessContinuity ): ?>
      <?php \NobleStudios\Utilities\Tools::renderPartial("listings/tabs/tab-business-continuity", $businessContinuity); ?>
    <?php endif; ?>

  </div>

  <?php if( $backLink ): ?>
    <div class="detail-tabs__button">
      <a href="<?php echo $backLink; ?>" class="button__link --stakeholder js-back-button">Back</a>
    </div>

    <script>
        // Script to send the user back to their previous page if coming from a VDP page (ie /offers), if previous page was external -> lodging listing page
        let lastPage = document.referrer;
        let currentDomain = window.location.hostname;
        let backButton = document.querySelector('.js-back-button');

        backButton.addEventListener('click', function(e) {
            e.preventDefault();

            if(lastPage.indexOf('https://' + currentDomain) > -1) {
                history.go(-1);
            } else {
                window.location.href = 'https://' + currentDomain + '/lodging';
            }
        });
    </script>

  <?php endif; ?>
</div>
