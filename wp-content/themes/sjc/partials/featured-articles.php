<?php
    use function NobleStudios\Helpers\getFeaturedImage;
    extract($partialData);

    if (empty($items)) return;
 ?>

<section>
  <div class="section background-cover">
    <div class="section__container">
          <!-- Linked blocks  -->
          <div class="featured">
            <div class="featured__header">
                <h2><?php echo $title ?? 'Featured Articles' ?></h2>
            </div>

            <div class="featured__blocks js-slider-mobile">
              
              <?php 
              $columns = '--'.count($items).'-columns';

              if( $items ): $counter = 1; ?>
              <?php foreach ($items as $linkedBlock): ?>

                <div class="image-block gm-block-<?php echo $counter; ?> <?php echo $columns ?>">
                  <a href="<?php echo get_permalink($linkedBlock->ID); ?>" class="image-block__container">
                    <div class="image-block__content">
                      <div class="image-block__image <?php echo $columns ?> js-background-cover background-cover">
                        <img src="<?php echo getFeaturedImage($linkedBlock->ID) ?>" alt="<?php echo wp_trim_words($linkedBlock->post_title, 7, '...'); ?>">
                      </div>
                      <div class="image-block__header">
                        <h3 class="sub-header"> <?php  echo wp_trim_words($linkedBlock->post_title, 7, '...'); ?> </h3>
                      </div>
                    </div>
                  </a>
                </div>
              <?php $counter++; endforeach; endif; ?>
            </div>

        </div>
    </div>
  </div>
</section>