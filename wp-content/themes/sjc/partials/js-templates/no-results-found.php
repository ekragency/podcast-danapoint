<script type="text/template" class="js-no-results">
  <div class="section">
    <div class="section__container">
      <center>
        No Results found. Try removing some search parameters.
      </center>
    </div>
  </div>
</script>
