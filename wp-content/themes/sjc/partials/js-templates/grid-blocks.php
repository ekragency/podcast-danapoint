<?php // Renders a stakeholder grid listing ?>
<script type="text/template" class="js-grid-block">
  <% if( !_.isEmpty(templateData) ) { %>
    <% var primaryCategory = templateData.stakeholder_custom_meta.primary_category ? templateData.stakeholder_custom_meta.primary_category.name : '' %>
    <% var tier = templateData.stakeholder_custom_meta.tier %>
    <% var tier_class = '' %>
    <% if( tier == 'Tier 2' ) { tier_class = '--tier2' } %>
    <div class="grid-block <%= tier_class %>">
      <a href="<%= templateData.link %>" class="grid-block__link">
        <div class="grid-block__image js-background-cover background-cover">
          <img src="<%= templateData.stakeholder_custom_meta.image ? templateData.stakeholder_custom_meta.image  : '' %>" alt="<%= templateData.title.rendered %>">
        </div>
        <div class="grid-block__header">
          <h5><%= templateData.title.rendered %></h5>
        </div>
        <div class="grid-block__bottom-text" data-bottomText="<%= primaryCategory %>">
          <%= primaryCategory %>
        </div>
      </a>
      <% if( templateData.stakeholder_custom_meta.offers ) { %>
      <span class="featured-ribbon"><img src="/wp-content/themes/visitdanapoint/assets/svgs/tag.svg" class="js-svg-swap"></span>
      <% } %>
    </div>
  <% } %>
</script>
