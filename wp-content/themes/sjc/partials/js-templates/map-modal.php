<script type="text/template" class="js-map-modal-template">
  <div class="map-modal js-map-modal --shown">
    <a href="#" class="map-modal__close js-map-modal-close"> close </a>
    <div class="map-modal__content">
      <div class="map-modal__image js-background-cover background-cover"> <img src="<%= templateData.stakeholder_custom_meta.image %>" /> </div>
      <div class="map-modal__meta">
        <a href="<%= templateData.link %>"> <h4 class="map-modal__title"> <%= templateData.title.rendered %> </h4> </a>
        <% if( templateData.stakeholder_custom_meta.primary_category && templateData.stakeholder_custom_meta.primary_category.name ) { %>
          <p> <%= templateData.stakeholder_custom_meta.primary_category.name %> </p>
        <% } %>
        <a href="<%= templateData.link %>" class="link__cta map-modal__more"> More Details </a>
      </div>
    </div>
  </div>
</script>
