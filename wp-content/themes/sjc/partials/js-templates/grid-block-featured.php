<?php // Render a 2x2 featured stakeholder listing ?>
<script type="text/template" class="js-grid-block-featured">
  <% if( !_.isEmpty(templateData) ) { %>
    <% var primaryCategory = templateData.stakeholder_custom_meta.primary_category ? templateData.stakeholder_custom_meta.primary_category.name : '' %>
    <div class="grid-block --featured">
      <a href="<%= templateData.link %>" class="grid-block__link">
        <div class="grid-block__image js-background-cover background-cover">
          <img src="<%= templateData.stakeholder_custom_meta.image ? templateData.stakeholder_custom_meta.image  : '' %>" alt="<%= templateData.title.rendered %>">
        </div>
        <div class="grid-block__header">
          <h4><%= templateData.title.rendered %></h4>
        </div>
        <div class="grid-block__bottom-text" data-bottomText="<%= primaryCategory %>">
          <%= primaryCategory %>
        </div>
      </a>
      <% if( templateData.stakeholder_custom_meta.offers ) { %>
      <span class="featured-ribbon"><img src="/wp-content/themes/visitdanapoint/assets/svgs/min/tag.svg" class="js-svg-swap"></span>
      <% } %>
    </div>
  <% } %>
</script>
