<?php // Renders a stakeholder grid listing ?>
<script type="text/template" class="js-grid-offers-block">
  <% if( !_.isEmpty(templateData) ) { %>
    <% var primaryCategory = templateData.stakeholder_custom_meta.primary_category ? templateData.stakeholder_custom_meta.primary_category.name : '' %>
    <% var tiered = templateData.stakeholder_special_offer.indexOf('Special Offer') > -1 %>
    <% var tier_class = '' %>
    <% if( tiered ) { tier_class = '--tier2' } %>

    <div class="grid-block <%= tier_class %> --offer">
      <a href="<%= templateData.stakeholder_offer_link %>" class="grid-block__link">
        <div class="grid-block__image js-background-cover background-cover">
          <img src="<%= templateData.stakeholder_offer_image ? templateData.stakeholder_offer_image  : '' %>" alt="<%= templateData.stakeholder_offer_name %>">
        </div>
        <div class="grid-block__header">
          <h5><%= templateData.stakeholder_offer_name %></h5>
        </div>
        <div class="grid-block__bottom-text" >
          <%= primaryCategory %>
          <span class="grid-block__sub-bottom-text"><%= templateData.title %></span>
        </div>
      </a>
    </div>
  <% } %>
</script>
