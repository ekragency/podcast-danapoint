<?php
    // This is used on all pages, posts, etc EXCEPT for the Homepage
    use NobleStudios\Utilities\Tools;

    extract($partialData);
 ?>
<section>
  <div class="section">
    <div class="section__container --full-width --no-top-padding --no-bottom-padding">
      <div class="hero">
        <div class="hero__container">
          <?php if ( !empty($text) ): ?>
              <div class="hero__container-text">
                  <p><?php echo $text ?></p>
              </div>
          <?php endif; ?>

          <?php if ( !empty($title) ): ?>
              <div class="hero__container-title">
                  <h1><?php echo $title ?></h1>
              </div>
          <?php endif; ?>

          <?php if ( !empty($title) && empty($description) ): ?>
              <div class="section__divider"></div>
          <?php endif; ?>

          <?php if ( !empty($description) ): ?>
              <div class="hero__container-description">
                  <p>
                    <?php echo $description ?>
                  </p>
              </div>
          <?php endif; ?>

          <?php if (!empty($cta['url']) && !empty($cta['title'])): ?>
              <a href="<?php echo $cta['url'] ?>" target="<?php echo $cta['target'] ?>" class="hero__container-button button__button"><?php echo $cta['title'] ?></a>
          <?php endif; ?>
        </div>
        <?php
          Tools::renderPartial("hero/hero-image", $partialData);
          Tools::renderPartial("hero/hero-overlay");
        ?>
      </div>
    </div>
  </div>
</section>
