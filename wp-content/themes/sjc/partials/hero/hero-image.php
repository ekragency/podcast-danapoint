<?php
  $mobile = '';
  $tablet = '';
  $desktop = '';
  $class = '';

  if(isset($partialData)) {
    $mobile = isset($partialData['mobile']) ? $partialData['mobile'] : '';
    $tablet = isset($partialData['tablet']) ? $partialData['tablet'] : '';
    $desktop = isset($partialData['desktop']) ? $partialData['desktop'] : '';
    $class = isset($partialData['class']) ? $partialData['class'] : '';
  }

  if( (!isset($desktop) || empty($desktop)) && (!isset($tablet) || empty($tablet)) && (!isset($mobile) || empty($mobile)) ) {
    if( get_field('default_hero_images', 'option') ) {
      $images = get_field('default_hero_images', 'option')[0];
      $mobile = isset($images['default_hero_image_mobile']) ? $images['default_hero_image_mobile'] : '';
      $tablet = isset($images['default_hero_image_tablet']) ? $images['default_hero_image_tablet'] : '';
      $desktop = isset($images['default_hero_image_desktop']) ? $images['default_hero_image_desktop'] : '';
    }
  }

  $heroImages = array();

  if(!empty($mobile)) {
    $heroImages[0] = $mobile;
    if(!empty($tablet)) {
      $heroImages[1] = $tablet;
      if(!empty($desktop)) {
        $heroImages[2] = $desktop;
      }
    } elseif (!empty($desktop)) {
        $heroImages[1] = $desktop;
      }
  } elseif (!empty($tablet)) {
    $heroImages[0] = $tablet;
    if(!empty($desktop)) {
      $heroImages[1] = $desktop;
    }
  } else {
    $heroImages[0] = $desktop;
  }
?>

<div class="hero__background js-background-cover background-cover <?php echo $class; ?>">
  <picture>
  <?php if(isset($heroImages[2])) : ?>
    <source srcset="<?php echo $heroImages[2]; ?>" media="(min-width: 1140px)">
  <?php endif; ?>
  <?php if(isset($heroImages[1])) : ?>
    <source srcset="<?php echo $heroImages[1]; ?>" media="(min-width: 450px)">
  <?php endif; ?>
  <?php if(isset($heroImages[0])) : ?>
    <img srcset="<?php echo $heroImages[0]; ?>" alt="Hero Image">
  <?php endif; ?>
  </picture>
</div>
