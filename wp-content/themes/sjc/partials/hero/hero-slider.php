<?php
    use NobleStudios\Utilities\Tools;

    extract($partialData);
?>
<!--slides -->
<?php if( $partialData['slider'] ) : ?>
    <div class="hero__slides js-slider">

        <?php foreach( $partialData['slider'] as $slide ): ?>
            <div class="hero__slide-container">

                <div class="hero__slide js-background-cover background-cover <?php echo $class; ?>">

                    <div class="hero__slide-content">
                        <?php if ( !empty( $slide['hero_slider_image_subheader'] ) ): ?>
                            <div class="hero__slide-content-text">
                                <p><?php echo $slide['hero_slider_image_subheader']; ?></p>
                            </div>
                        <?php endif; ?>

                        <?php if ( !empty( $slide['hero_slider_image_header'] ) ): ?>
                            <div class="hero__slide-content-title">
                                <h1><?php echo $slide['hero_slider_image_header']; ?></h1>
                            </div>
                            <div class="section__divider"></div>
                        <?php endif; ?>

                        <?php if ( !empty( $slide['hero_slider_image_button_label'] ) && !empty( $slide['hero_slider_image_button_link'] ) ) : ?>
                            <a href="<?php echo $slide['hero_slider_image_button_link']; ?>" target="_self" class="hero__container-button button__button"><?php echo $slide['hero_slider_image_button_label']; ?></a>
                        <?php endif; ?>
                    </div>

                  <picture>
                    <source srcset="<?php echo $slide['hero_slider_image_desktop']['url']; ?>" media="(min-width: 1024px)">
                    <source srcset="<?php echo $slide['hero_slider_image_tablet']['url']; ?>" media="(min-width: 768px)">
                    <img srcset="<?php echo $slide['hero_slider_image_mobile']['url']; ?>" alt="<?php echo $slide['hero_slider_image_mobile']['alt']; ?>">
                  </picture>

                  <?php Tools::renderPartial("hero/hero-overlay"); ?>

              </div>

          </div>
      <?php endforeach; ?>

  </div>
<?php endif; ?>
