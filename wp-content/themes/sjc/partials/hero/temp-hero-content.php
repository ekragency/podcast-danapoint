<?php if ( !empty( $slide['hero_slider_image_subheader'] ) ): ?>
							<div class="hero__container-text">
									<p><?php echo $slide['hero_slider_image_subheader']; ?></p>
							</div>
					<?php endif; ?>

					<?php if ( !empty( $slide['hero_slider_image_header'] ) ): ?>
							<div class="hero__container-title">
									<h1><?php echo $slide['hero_slider_image_header']; ?></h1>
							</div>
							<div class="section__divider"></div>
					<?php endif; ?>

					<?php if ( !empty( $slide['hero_slider_image_button_label'] ) && !empty( $slide['hero_slider_image_button_link'] ) ) : ?>
							<a href="<?php echo $slide['hero_slider_image_button_link']; ?>" target="_self" class="hero__container-button button__button"><?php echo $slide['hero_slider_image_button_label']; ?></a>
					<?php endif; ?>
