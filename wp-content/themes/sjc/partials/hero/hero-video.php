<?php
  if(isset($partialData)) {
    $videoId = $partialData['video'];
  }

  if( get_field('default_hero_images', 'option') ) {
    $images = get_field('default_hero_images', 'option')[0];
    $mobile = isset($images['default_hero_image_mobile']) ? $images['default_hero_image_mobile'] : '';
    $tablet = isset($images['default_hero_image_tablet']) ? $images['default_hero_image_tablet'] : '';
    $desktop = isset($images['default_hero_image_desktop']) ? $images['default_hero_image_desktop'] : '';
  }

?>
<div class="hero__video">
  <div id="hero-video" data-videoid="<?php echo $videoId; ?>"></div>
  <div class="hero__video-image --hide js-video-image js-background-cover background-cover">
    <picture>
      <source srcset="<?php echo $desktop; ?>" media="(min-width: 1140px)">
      <source srcset="<?php echo $tablet; ?>" media="(min-width: 450px)">
      <img srcset="<?php echo $mobile; ?>" alt="Visit Dana Point">
    </picture>
  </div>
</div>
