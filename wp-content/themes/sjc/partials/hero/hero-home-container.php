<?php
    // This is used on the homepage only and allows for video or a slider or defaults to the global hero images if nothing is set
    use NobleStudios\Utilities\Tools;

    extract($partialData);
?>
<section>
  <div class="section">
    <div class="section__container --full-width --no-top-padding --no-bottom-padding">
      <div class="hero">
        <?php
            Tools::renderPartial("hero/hero-slider", $partialData);
        ?>
      </div>
    </div>
  </div>
</section>
