<?php
    // This is used on all pages, posts, etc EXCEPT for the Homepage
    use NobleStudios\Utilities\Tools;

    extract($partialData);
 ?>
<section>
  <div class="section">
    <div class="section__container --full-width --no-top-padding --no-bottom-padding">
      <div class="hero">
        <div class="hero__container" style="bottom: 15%;">

          <div class="hero__container-text">
              <p>Let the Good Times Roll</p>
          </div>

          <div class="hero__container-title">
              <h1 class="--stake-listing">Dana Point Events</h1>
          </div>

          <div class="hero__container-description">
              <p>
                  Dana Point events range from rocking music festivals on the beach to surfing competitions. Good times and good vibes can be found nightly at Dana Point bars and music venues. Head to one of our oceanfront resorts for their iconic events like Yappy Hour—a shindig for you and your furry friend. Stroll the Farmers Market on Saturday mornings or take a walk through history at the Dana Point Nature Interpretive Center.
              </p>
          </div>

        </div>
        <?php
        if( get_field('default_hero_images', 'option') ) {
          $images = get_field('default_hero_images', 'option')[0];
          $mobile = isset($images['default_hero_image_mobile']) ? $images['default_hero_image_mobile'] : '';
          $tablet = isset($images['default_hero_image_tablet']) ? $images['default_hero_image_tablet'] : '';
          $desktop = isset($images['default_hero_image_desktop']) ? $images['default_hero_image_desktop'] : '';
        }

        $heroImages = array();

        if(!empty($mobile)) {
          $heroImages[0] = $mobile;
          if(!empty($tablet)) {
            $heroImages[1] = $tablet;
            if(!empty($desktop)) {
              $heroImages[2] = $desktop;
            }
          } elseif (!empty($desktop)) {
              $heroImages[1] = $desktop;
            }
        } elseif (!empty($tablet)) {
          $heroImages[0] = $tablet;
          if(!empty($desktop)) {
            $heroImages[1] = $desktop;
          }
        } else {
          $heroImages[0] = $desktop;
        }
        ?>

        <div class="hero__background js-background-cover background-cover --stake-listing">
          <picture>
          <?php if(isset($heroImages[2])) : ?>
            <source srcset="<?php echo $heroImages[2]; ?>" media="(min-width: 1140px)">
          <?php endif; ?>
          <?php if(isset($heroImages[1])) : ?>
            <source srcset="<?php echo $heroImages[1]; ?>" media="(min-width: 450px)">
          <?php endif; ?>
          <?php if(isset($heroImages[0])) : ?>
            <img srcset="<?php echo $heroImages[0]; ?>" alt="Hero Image">
          <?php endif; ?>
          </picture>
        </div>
        <?php
          Tools::renderPartial("hero/hero-stake-overlay");
        ?>
      </div>
    </div>
  </div>
</section>
