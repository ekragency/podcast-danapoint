<?php
  if(isset($partialData)) {
    $videoId = $partialData['video'];


  }

?>
<?php if (!empty($partialData['video']['vimeo-id'])) : ?>

<div class="hero__video --no-crop">
  <iframe src="https://player.vimeo.com/video/<?= $partialData['video']['vimeo-id'] ?>?background=1&amp;badge=0&amp;autopause=0&amp;controls=0&amp;muted=1&amp;player_id=0&amp;autoplay=1&amp;loop=1" frameborder="0" controls="0" allow="autoplay"></iframe>
</div>
<script src="https://player.vimeo.com/api/player.js"></script>
<?php endif; ?>

