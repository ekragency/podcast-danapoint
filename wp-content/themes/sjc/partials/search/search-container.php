<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
   $total_posts = $wp_query->found_posts;
   $start_post = ($paged - 1) * $posts_per_page + 1;
   $end_post = min($start_post + $posts_per_page - 1, $total_posts);
?>
<section>
  <div class="section">
    <div class="section__container">
      <div class="search-page">
        <div class="search-page__count-container">
          <div class="search-page__header">
            <h1>Search Results</h1>
          </div>
          <?php if( $total_posts > 0 ): ?>
            <div class="search-page__count"><span class="search-page__bold"><?php echo $start_post; ?>-<?php echo $end_post; ?></span> of <span class="search-page__bold"><?php echo $total_posts; ?></span> results for "<?php echo get_search_query(); ?>"</div>
          <?php else: ?>
            <div class="search-page__count"><span class="search-page__bold"><?php echo $total_posts; ?></span> results for "<?php echo get_search_query(); ?>"</div>
          <?php endif; ?>
        </div>
        <div class="search-page__content">

          <div class="search-page__search-form">
            <?php get_search_form (); ?>
          </div>

          <?php \NobleStudios\Utilities\Tools::renderPartial("search/search-results");?>

          <?php \NobleStudios\Utilities\Tools::renderPartial("search/search-pagination");?>

        </div>
      </div>
    </div>
  </div>
</section>
