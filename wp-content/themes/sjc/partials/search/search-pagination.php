<?php
  $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
  $pages = $wp_query->max_num_pages;
?>
<div class="site-pagination">
  <?php if( $pages > 0 ): ?>
    <div class="site-pagination__text"> Page <?php echo $paged; ?> of <?php echo $pages; ?></div>
  <?php endif; ?>
  <div class="site-pagination__buttons">
  <?php
    the_posts_pagination( array(
      'mid_size'  => 2,
      'prev_text' => __( 'Prev', 'textdomain' ),
      'next_text' => __( 'Next', 'textdomain' ),
  ) );
  ?>
  </div>
</div>
