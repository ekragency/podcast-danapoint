<div class="search-page__results">
  <ul class="search-results__items">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

     <li class="search-results__item">
       <article>
       <div class="search-results__item-container">
        <?php if(has_post_thumbnail() && empty(get_the_post_thumbnail_url()) == false) : ?>
           <div class="search-results__image-block">
             <aside>
              <a href="<?php the_permalink(); ?>"><img src="<?php the_post_thumbnail_url(); ?>"class="search-results__image" alt=""></a>
             </aside>
           </div>
          <?php endif; ?>
           <div class="search-results__content">
             <div class="search-results__header"><a href="<?php the_permalink(); ?>"><h4 class="sub-header-alt"><?php the_title(); ?></h4></a></div>
               <?php the_excerpt();?>
           </div>
         </div>
       </article>
     </li>

<?php endwhile; else : ?>
    <li><p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p></li>
<?php endif; ?>
  </ul>
</div>
